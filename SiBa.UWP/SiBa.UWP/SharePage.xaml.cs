﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using System.Text.RegularExpressions;
using Windows.UI.Popups;
using System.Net.Http;
using Windows.UI.Xaml.Documents;
using System.Threading.Tasks;


// The Blank Page item template is documented at https://go.microsoft.com/fwlink/?LinkId=234238

namespace SiBa.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SharePage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();

        public SharePage()
        {
            this.InitializeComponent();
            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;



        }
        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedTo(e);
        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
        }

        #endregion

        async Task<Boolean> SendEmailAndWait(string url)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            String msg = ""; 
            if (response.IsSuccessStatusCode == true)
            {
                msg = "Die Daten wurden an Deutschland sicher im Netz weitergeleitet.";
                await ShowAlert(msg);
                return true;
            }
            else
            {
                msg  = "Es ist beim Senden ein Fehler aufgetreten. Bitte versuchen Sie es zu einem späteren Zeitpunkt erneut.";
                await ShowAlert(msg);
                return false;
            }
        }

        private async void SendButton_Click(object sender, RoutedEventArgs e)
        {
            var fromField = this.from;
            var subjectField = this.subject;
            var linkField = this.link;
            var linkMessage = "";
            this.message.Document.GetText(Windows.UI.Text.TextGetOptions.None,out linkMessage);
            String from = fromField.Text;
            if (from == "") { from = "benutzerhatkeineadresseangegeben@siba.com"; }

            if (Regex.Match(from, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {

            }
            else
            {
                await ShowAlert("Die eigegebene Emailadresse scheint ungültig zu sein.");
                return;
            }


            bool isAllEmpty = true;


            string title = subjectField.Text;
            if (title == "") { title = "Benutzer hat keinen Titel angegeben."; } else { isAllEmpty = false; }

            string link = linkField.Text;
            if (link == "") { link = "Benutzer hat keinen Link angegeben."; } else { isAllEmpty = false; }

            string body = linkMessage;
            if (body == "") { body = "Benutzer hat kein Nachricht angegeben."; } else { isAllEmpty = false; }

            string code = SiBa.Keys.SendGridCode;
            string url = SiBa.Keys.SendGridURL;
            string to = "siba@dsin.de";

            if (!isAllEmpty)
            {
                string RestUrl = url + "code=" + code + "&from=" + from + "&to=" + to + "&link=" + link + "&title=" + title + "&message=" + body;
                Boolean ok = await SendEmailAndWait(RestUrl);
                if (ok) {
                    Frame.Navigate(typeof(MainPage), ((FrameworkElement)sender).Tag);
                }
            }
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(MainPage), ((FrameworkElement)sender).Tag);
        }

        async Task<int> ShowAlert(String msg)
        {
            MessageDialog messageDialog = new MessageDialog(msg);
            await messageDialog.ShowAsync();
            return 1;
        }
    }



}

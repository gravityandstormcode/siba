﻿using SiBa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace SiBa.UWP
{
    class LinkStyleToNotVisible : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, string culture)
        {
            return (Link.LinkStyle)value == Link.LinkStyle.TitleLinkNoLink ? Visibility.Visible : Visibility.Collapsed;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}

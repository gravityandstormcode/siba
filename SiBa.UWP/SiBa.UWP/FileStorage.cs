﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;
using SiBa;
using Windows.Storage;
using Windows.Storage.Streams;
using System.Text;
using System.Threading.Tasks;

namespace SiBa.UWP
{
	public class FileStorage : IFileStorage
	{
        private bool _isFlushing=false;

        public async Task<bool> Load()
        {
            string documents = ApplicationData.Current.LocalFolder.Path;
            string filename = Path.Combine(documents, "Assets","Settings.json");
            try
            {
                System.Diagnostics.Debug.WriteLine(filename);

                string output = await ReadTextFile("Settings.json");
                _cache = JsonConvert.DeserializeObject<Data>(output); // uncomment for debugging to get empty settings
                if (_cache == null)
                {
                    _cache = new Data();
                }
            }
            catch (Exception)
            {
                _cache = new Data();
                return false;
            }
            return true;
        }

        override async public void Flush (){
            if (!_isFlushing && _isDirty)
            {
                _isFlushing = true;
                try
                {
                    _isDirty = false;
                    string output = JsonConvert.SerializeObject(_cache);
                    String Path = await WriteTextFile("Settings.json", output);
                }
                catch (Exception e)
                {
                    System.Diagnostics.Debug.WriteLine(e.ToString());
                }
                _isFlushing = false;
                // if someone else stored data in the cache while we flushed the cache, do it again!
                if (_isDirty)
                {
                    Flush();
                }
            }
            else {

            }          
		}


        // Write a text file to the app’s local folder.
        public static async Task<string> WriteTextFile(string filename, string contents)
        {
            StorageFolder localFolder = ApplicationData.Current.LocalFolder;
            StorageFile textFile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);

            using (IRandomAccessStream textStream = await textFile.OpenAsync(FileAccessMode.ReadWrite))
            {
                using (DataWriter textWriter = new DataWriter(textStream))
                {
                    textWriter.WriteString(contents);
                    await textWriter.StoreAsync();
                }
            }

            return textFile.Path;
        }


        // Read the contents of a text file from the app’s local folder.
        public static async Task<string> ReadTextFile(string filename)
        {
            try
            {
                string contents;
                StorageFolder localFolder = ApplicationData.Current.LocalFolder;
                StorageFile textFile = await localFolder.GetFileAsync(filename);

                using (IRandomAccessStream textStream = await textFile.OpenReadAsync())
                {
                    using (DataReader textReader = new DataReader(textStream))
                    {
                        uint textLength = (uint)textStream.Size;
                        await textReader.LoadAsync(textLength);
                        contents = textReader.ReadString(textLength);
                    }
                }
                return contents;
            } catch(Exception) {
                // File did not exist, so we create an empty one now.
                StorageFolder localFolder = ApplicationData.Current.LocalFolder;
                StorageFile newFile = await localFolder.CreateFileAsync(filename, CreationCollisionOption.ReplaceExisting);
                return "";
            }
        }

    }
}


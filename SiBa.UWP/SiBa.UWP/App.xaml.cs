﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using System.Threading.Tasks;
using Windows.ApplicationModel;
using Windows.ApplicationModel.Activation;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Networking.PushNotifications;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Animation;
using Windows.UI.Xaml.Navigation;

using Microsoft.WindowsAzure.Messaging;
using Windows.UI.Popups;
using Windows.UI.Core;
using Windows.Storage;

// The Blank Application template is documented at http://go.microsoft.com/fwlink/?LinkId=234227

namespace SiBa.UWP
{
    /// <summary>
    /// Provides application-specific behavior to supplement the default Application class.
    /// </summary>
    public sealed partial class App : Application
    {
#if WINDOWS_PHONE_APP
        private TransitionCollection transitions;
#endif
        public static bool Filter = false;

        /// <summary>
        /// Initializes the singleton application object.  This is the first line of authored code
        /// executed, and as such is the logical equivalent of main() or WinMain().
        /// </summary>
        public App()
        {
            //this.InitializeComponent();
            this.Suspending += this.OnSuspending;
        }

        /// <summary>
        /// Invoked when the application is launched normally by the end user.  Other entry points
        /// will be used when the application is launched to open a specific file, to display
        /// search results, and so forth.
        /// </summary>
        /// <param name="e">Details about the launch request and process.</param>
        protected override async void OnLaunched(LaunchActivatedEventArgs e)
        {
#if DEBUG
            if (System.Diagnostics.Debugger.IsAttached)
            {
                this.DebugSettings.EnableFrameRateCounter = false;
            }
#endif

            Frame rootFrame = Window.Current.Content as Frame;

            // Do not repeat app initialization when the Window already has content,
            // just ensure that the window is active
            if (rootFrame == null)
            {
                // Create a Frame to act as the navigation context and navigate to the first page
                rootFrame = new Frame();

                // TODO: change this value to a cache size that is appropriate for your application
                rootFrame.CacheSize = 1;

                // Set the default language
                rootFrame.Language = Windows.Globalization.ApplicationLanguages.Languages[0];

                rootFrame.NavigationFailed += OnNavigationFailed;

                if (rootFrame.Content == null)
                {

                    System.Runtime.InteropServices.Architecture system = System.Runtime.InteropServices.RuntimeInformation.ProcessArchitecture;


                    if (system == System.Runtime.InteropServices.Architecture.Arm || system == System.Runtime.InteropServices.Architecture.Arm64)
                    {
                        rootFrame.Navigate(typeof(MainPageMobile));
                    }
                    else {
                        rootFrame.Navigate(typeof(MainPage));
                    }
                }

                // Place the frame in the current Window
                Window.Current.Content = rootFrame;
            }

            if (rootFrame.Content == null)
            {
#if WINDOWS_PHONE_APP
                // Removes the turnstile navigation for startup.
                if (rootFrame.ContentTransitions != null)
                {
                    this.transitions = new TransitionCollection();
                    foreach (var c in rootFrame.ContentTransitions)
                    {
                        this.transitions.Add(c);
                    }
                }

                rootFrame.ContentTransitions = null;
                rootFrame.Navigated += this.RootFrame_FirstNavigated;
#endif

                // When the navigation stack isn't restored navigate to the first page,
                // configuring the new page by passing required information as a navigation
                // parameter
                if (!rootFrame.Navigate(typeof(MainPage), e.Arguments))
                {
                    throw new Exception("Failed to create initial page");
                }
            }

            // Ensure the current window is active
            Window.Current.Activate();

            await InitNotificationsAsync();

            ((App)App.Current).UpdatePushSettings();
        }

#if WINDOWS_PHONE_APP
        /// <summary>
        /// Restores the content transitions after the app has launched.
        /// </summary>
        /// <param name="sender">The object where the handler is attached.</param>
        /// <param name="e">Details about the navigation event.</param>
        private void RootFrame_FirstNavigated(object sender, NavigationEventArgs e)
        {
            var rootFrame = sender as Frame;
            rootFrame.ContentTransitions = this.transitions ?? new TransitionCollection() { new NavigationThemeTransition() };
            rootFrame.Navigated -= this.RootFrame_FirstNavigated;
        }
#endif

        /// <summary>
        /// Invoked when application execution is being suspended.  Application state is saved
        /// without knowing whether the application will be terminated or resumed with the contents
        /// of memory still intact.
        /// </summary>
        /// <param name="sender">The source of the suspend request.</param>
        /// <param name="e">Details about the suspend request.</param>
        private void OnSuspending(object sender, SuspendingEventArgs e)
        {
            var deferral = e.SuspendingOperation.GetDeferral();

            // TODO: Save application state and stop any background activity
            deferral.Complete();
        }

        /// <summary>
        /// Invoked when Navigation to a certain page fails
        /// </summary>
        /// <param name="sender">The Frame which failed navigation</param>
        /// <param name="e">Details about the navigation failure</param>
        void OnNavigationFailed(object sender, NavigationFailedEventArgs e)
        {
            throw new Exception("Failed to load Page " + e.SourcePageType.FullName);
        }

        private async void OnPushNotificationReceived(PushNotificationChannel sender, PushNotificationReceivedEventArgs args)
        {
            var msg = args.ToastNotification.Content.InnerText;
            Debug.WriteLine($"Token: {msg}");
            // No dialogs for background functions ...
            // var dialog = new MessageDialog("Push received: " + msg);
            // dialog.Commands.Add(new UICommand("OK"));
            // await dialog.ShowAsync();

            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                SiBaArticles.GetInstance().Load();
            });
        }

        public async void UpdatePushSettings() {

            // load a setting that is local to the device
            ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            String PushToken = localSettings.Values["pushtoken"] as string;

            // replace this with the value from the settings screen
            List<string> tagCollection = new List<string>();

            SiBaSettings settings = SiBaSettings.GetInstance();
            if (settings.InitDone == false) {
                Task.Delay(TimeSpan.FromSeconds(3)).ContinueWith(_ => UpdatePushSettings());
                return;
            }

            Boolean Red = settings.GetDangerState(2);
            Boolean Yellow = settings.GetDangerState(1);
            Boolean Green = settings.GetDangerState(0);

            List<SiBaCategory> Categories = settings.GetCategories();
            foreach (SiBaCategory category in Categories)
            {
                try
                {
                    category.State = settings.GetCategoryState(category.Id);
                    if (category.State == true) {
                        if (Red == true)
                        {
                            tagCollection.Add("red_" + category.Id.ToString());
                        }
                        if (Yellow == true)
                        {
                            tagCollection.Add("yellow_" + category.Id.ToString());
                        }
                        if (Green == true)
                        {
                            tagCollection.Add("green_" + category.Id.ToString());
                        }
                    }
                }
                catch (Exception)
                {

                }
            }


            String Debug = localSettings.Values["debug"] as String;
            if (Debug == "true")
            {
                tagCollection.Add("debug_debug");
            }

            var hub = new NotificationHub(SiBa.Keys.NotificationHubName, SiBa.Keys.NotificationHubEndpoint);
            var toastTemplate = @"<toast><visual><binding template=""ToastText01""><text id=""1"">$(message)</text></binding></visual></toast>";
            //var registration = new WindowsTemplateRegistrationDescription(deviceUpdate.Handle, toastTemplate);
            var reg = await hub.RegisterTemplateAsync(PushToken, toastTemplate, "SiBaTemplate");
            var result = await hub.RegisterNativeAsync(PushToken, tagCollection);



        }

        private async Task InitNotificationsAsync()
        {
            var channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();
            channel.PushNotificationReceived += OnPushNotificationReceived;
            Debug.WriteLine($"Received Token: {channel.Uri}");

            // Save the token
            ApplicationDataContainer localSettings = Windows.Storage.ApplicationData.Current.LocalSettings;
            localSettings.Values["pushtoken"] = channel.Uri;

            UpdatePushSettings();

            // Displays the registration ID so you know it was successful
            // DEBUG ONLY
            //if (result.RegistrationId != null)
            //{
            //var dialog = new MessageDialog("Registration successful: " + result.RegistrationId);
            //dialog.Commands.Add(new UICommand("OK"));
            //await dialog.ShowAsync();
            //}
        }

    }
}
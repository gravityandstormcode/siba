﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media.Imaging;

namespace SiBa.UWP
{
    class ImagePathConverter : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, string culture)
        {
            try
            {
                return new BitmapImage(new Uri("ms-appx:///Assets/danger-" + value + ".png"));
            }
            catch
            {
                throw new NotImplementedException();
            }
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}

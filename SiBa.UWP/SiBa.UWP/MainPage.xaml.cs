﻿using SiBa;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.ApplicationModel;
using Windows.UI.Core;
using Windows.ApplicationModel.Background;
using Windows.Networking.PushNotifications;
using Windows.UI.Notifications;
using Windows.Data.Xml.Dom;
using Plugin.Share;
using Windows.ApplicationModel.DataTransfer;


// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SiBa.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPage : Page
    {
        private FileStorage _storage;
        protected SiBaArticlesFilter _filter;

        public MainPage()
        {
            this.InitializeComponent();

            DataTransferManager dataTransferManager = DataTransferManager.GetForCurrentView();
            dataTransferManager.DataRequested += new TypedEventHandler<DataTransferManager, DataRequestedEventArgs>(dataTransferManager_DataRequested);
            this._filter = new SiBaArticlesFilter();
            this._filter.Filter();

        }




        private void dataTransferManager_DataRequested(DataTransferManager sender, DataRequestedEventArgs e)
        {
            if (this.ArticleDetail.DataContext != null)
            {
                DataPackage requestData = e.Request.Data;
                SiBa.SiBaArticle article = (SiBa.SiBaArticle)this.ArticleDetail.DataContext;
                requestData.Properties.Title = article.Title;
                requestData.SetText(article.Title);
                requestData.SetUri(new Uri("https://www.sicher-im-netz.de/node/" + article.Id.ToString()));
            }
        }

        /*
        private async void initNotifications()
        {
            /*
            try
            {

                PushNotificationChannel channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

                // Send channel url to server
                var token = channel.Uri.ToString();
                System.Diagnostics.Debug.WriteLine(token);
                SiBaNotification notification = new SiBaNotification();
                await notification.registerToken(token);

                channel.PushNotificationReceived += OnPushNotification;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }

        private async void OnPushNotification(PushNotificationChannel sender, PushNotificationReceivedEventArgs e)
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText01;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
            XmlNodeList textElements = toastXml.GetElementsByTagName("text");
            string test = e.ToastNotification.Content.InnerText;
//            ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(toastXml));



            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                // Code to run here
                SiBaArticles.GetInstance().Load();
            }
            );
            //            e.Cancel = true;
        }
    */
        public void OnArticlesLoaded(List<SiBaArticle> articles)
        {
            this._filter.Filter();
            this.MainArticles.ItemsSource = this._filter.FilteredArticles;
            //this.MainArticles.ItemsSource = articles;
            ProgressBar.IsActive = false;
            _storage.Flush();
            if (this.MainArticles.Items.Count > 0)
            {
                this.MainArticles.SelectedIndex = 0;
            }
            
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SiBaArticles articles = SiBaArticles.GetInstance();
            this._filter.Filter();

            //            await MobileService.InitChannel();
            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void OnLinkClicked(object sender, TappedRoutedEventArgs e)
        {
            // Frame.Navigate(typeof(WebView), ((FrameworkElement)sender).Tag);
        }

        private void OnDetailViewClicked(object sender, ItemClickEventArgs e)
        {
//            this.ArticleDetail.DataContext = e.ClickedItem;
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            App.Filter = true;
            Frame.Navigate(typeof(SettingsPage), ((FrameworkElement)sender).Tag);
        }

        private async void TeilenButton_Click(object sender, RoutedEventArgs e)
        {
            var title = ""; 
            var message = "";
            var url = "";

            if (this.ArticleDetail.DataContext != null) {
                SiBa.SiBaArticle article = (SiBa.SiBaArticle)this.ArticleDetail.DataContext;
                title = article.Title;
                message = article.Title;
                url = "https://www.sicher-im-netz.de/node/" + article.Id.ToString();
            }

            // Share a link and an optional title and message.
            await CrossShare.Current.ShareLink(url, message, title);

            CrossShare.Current.Share(message);
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            App.Filter = false;
            Frame.Navigate(typeof(SettingsPage), ((FrameworkElement)sender).Tag);
        }

        private void PartnerButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(PartnerPage), ((FrameworkElement)sender).Tag);
        }
        private void ImprintAndPrivacy_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ImprintAndPrivacyPage), ((FrameworkElement)sender).Tag);
        }

        private void MainArticles_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if ((sender as ListView).SelectedItem != null)
            {
                this.ArticleDetail.DataContext = (sender as ListView).SelectedItem;
            }
            if (e.AddedItems.Count == 0) (sender as ListView).SelectedItem = e.RemovedItems[0];
        }

        private void ShareButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SharePage), ((FrameworkElement)sender).Tag);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            CommandBar bottomAppBar = this.BottomAppBar as CommandBar;

            if (bottomAppBar != null)
            {
                // SymbolIcon
                AppBarButton button0 = new AppBarButton();
                button0.Icon = new SymbolIcon(Symbol.Filter);
                button0.Label = "Filter";
                button0.Click += FilterButton_Click;
                button0.IsCompact = false;
                bottomAppBar.PrimaryCommands.Add(button0);

                AppBarButton button1 = new AppBarButton();
                button1.Icon = new SymbolIcon(Symbol.Share);
                button1.Label = "Teilen";
                button1.Click += TeilenButton_Click;
                button1.IsCompact = false;
                bottomAppBar.PrimaryCommands.Add(button1);

                AppBarButton button2 = new AppBarButton();
                button2.Icon = new SymbolIcon(Symbol.Setting);
                button2.Label = "Einstellungen";
                button2.Click += SettingsButton_Click;
                button2.IsCompact = false;
                bottomAppBar.PrimaryCommands.Add(button2);

                AppBarButton button3 = new AppBarButton();
                button3.Icon = new SymbolIcon(Symbol.More);
                button3.Label = "Partner";
                button3.Click += PartnerButton_Click;
                button3.IsCompact = false;
                bottomAppBar.PrimaryCommands.Add(button3);

                AppBarButton button4 = new AppBarButton();
                button4.Icon = new SymbolIcon(Symbol.Help);
                button4.Label = "Impressum & Datenschutz";
                button4.Click += ImprintAndPrivacy_Click;
                button4.IsCompact = false;
                bottomAppBar.PrimaryCommands.Add(button4);

                AppBarButton button5 = new AppBarButton();
                button5.Icon = new SymbolIcon(Symbol.Edit);
                button5.Label = "Nachricht an DsiN senden";
                button5.Click += ShareButton_Click;
                button5.IsCompact = false;
                bottomAppBar.PrimaryCommands.Add(button5);
            }


            SiBaSettings settings = SiBaSettings.GetInstance();
            if (settings.storage == null) {
                _storage = new FileStorage();
                await _storage.Load();
                Tracking.GetInstance().Track("Open", "Main");
                SiBaSettings.GetInstance().Init(_storage, "windows");
                SiBaSettings.GetInstance().SummaryCutoff = 40;
                settings.InitDone = true;
            }
            _storage = (FileStorage) SiBaSettings.GetInstance().storage;
            SiBaArticles articles = SiBaArticles.GetInstance();
            SiBaArticles.OnDataLoaded loaded = new SiBaArticles.OnDataLoaded(OnArticlesLoaded);
            SiBaCategories.GetInstance().Load();
            articles.AddOnFinishedCallback(loaded);

            articles.Load();
            this.NavigationCacheMode = NavigationCacheMode.Required;

            Application.Current.Suspending += new SuspendingEventHandler((x, y) => {
                Tracking.GetInstance().Track("App Shutdown");
            });
            // initNotifications();

        }
    }
}

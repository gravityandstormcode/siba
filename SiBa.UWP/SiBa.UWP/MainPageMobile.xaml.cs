﻿using SiBa;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.UI;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Media.Imaging;
using Windows.UI.Xaml.Navigation;
using Windows.Networking.PushNotifications;
using Windows.ApplicationModel;
using Windows.UI.Core;
using Windows.ApplicationModel.Background;
using Windows.UI.Notifications;
using Windows.Data.Xml.Dom;
using Plugin.Share;

// The Blank Page item template is documented at http://go.microsoft.com/fwlink/?LinkId=234238

namespace SiBa.UWP
{

    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class MainPageMobile : Page
    {
        private FileStorage _storage;
        private SiBa.SiBaArticle selectedArticle;
        protected SiBaArticlesFilter _filter;


        public MainPageMobile()
        {
            this.InitializeComponent();
            Application.Current.Suspending += new SuspendingEventHandler((x, y) => {
                Tracking.GetInstance().Track("App Shutdown");
            });
            this._filter = new SiBaArticlesFilter();
            this._filter.Filter();


        }


        /*
        private async void initNotifications()
        {
            try
            {
                PushNotificationChannel channel = await PushNotificationChannelManager.CreatePushNotificationChannelForApplicationAsync();

                // Send channel url to server
                var token = channel.Uri.ToString();
                System.Diagnostics.Debug.WriteLine(token);
                SiBaNotification notification = new SiBaNotification();
                await notification.registerToken(token);

                channel.PushNotificationReceived += OnPushNotification;
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e.ToString());
            }
        }

        private async void OnPushNotification(PushNotificationChannel sender, PushNotificationReceivedEventArgs e)
        {
            ToastTemplateType toastTemplate = ToastTemplateType.ToastText01;
            XmlDocument toastXml = ToastNotificationManager.GetTemplateContent(toastTemplate);
            XmlNodeList textElements = toastXml.GetElementsByTagName("text");
            textElements[0].AppendChild(toastXml.CreateTextNode(e.ToastNotification.Content.InnerText));
            ToastNotificationManager.CreateToastNotifier().Show(new ToastNotification(toastXml));



            await Windows.ApplicationModel.Core.CoreApplication.MainView.CoreWindow.Dispatcher.RunAsync(CoreDispatcherPriority.Normal, () =>
            {
                // Code to run here
                SiBaArticles.GetInstance().Load();
            }
            );
            //            e.Cancel = true;
        }

    */

        public void OnArticlesLoaded(List<SiBaArticle> articles)
        {
            this._filter.Filter();
            this.MainArticles.ItemsSource = this._filter.FilteredArticles;
            //this.MainArticles.ItemsSource = articles;
            ProgressBar.IsActive = false;
            _storage.Flush();
        }

        /// <summary>
        /// Invoked when this page is about to be displayed in a Frame.
        /// </summary>
        /// <param name="e">Event data that describes how this page was reached.
        /// This parameter is typically used to configure the page.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            SiBaArticles articles = SiBaArticles.GetInstance();
            this._filter.Filter();

            //            await MobileService.InitChannel();
            // TODO: If your application contains multiple pages, ensure that you are
            // handling the hardware Back button by registering for the
            // Windows.Phone.UI.Input.HardwareButtons.BackPressed event.
            // If you are using the NavigationHelper provided by some templates,
            // this event is handled for you.
        }

        private void OnLinkClicked(object sender, TappedRoutedEventArgs e)
        {

            Frame.Navigate(typeof(WebView), ((FrameworkElement)sender).Tag);
        }

        private void OnDetailViewClicked(object sender, TappedRoutedEventArgs e)
        {
            Frame.Navigate(typeof(DetailPage), (SiBaArticle)((FrameworkElement)sender).DataContext);
            this.selectedArticle = (SiBaArticle)((FrameworkElement)sender).DataContext;
        }

        private void FilterButton_Click(object sender, RoutedEventArgs e)
        {
            App.Filter = true;
            Frame.Navigate(typeof(SettingsPage), ((FrameworkElement)sender).Tag);
        }

        private void HardwareButtonsNeu_BackPressed(object sender, Windows.UI.Core.BackRequestedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
                e.Handled = true;
            }
        }

        private void HardwareButtons_BackPressed(object sender, KeyboardAcceleratorInvokedEventArgs e)
        {
            if (Frame.CanGoBack)
            {
                Frame.GoBack();
                e.Handled = true;
            }
        }

        private void SettingsButton_Click(object sender, RoutedEventArgs e)
        {
            App.Filter = false;
            Frame.Navigate(typeof(SettingsPage), ((FrameworkElement)sender).Tag);
        }
        private void ShareButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(SharePage), ((FrameworkElement)sender).Tag);
        }

        private void PartnerButton_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(PartnerPage), ((FrameworkElement)sender).Tag);
        }

        private void ImprintAndPrivacy_Click(object sender, RoutedEventArgs e)
        {
            Frame.Navigate(typeof(ImprintAndPrivacyPage), ((FrameworkElement)sender).Tag);
        }

        private async void Page_Loaded(object sender, RoutedEventArgs e)
        {
            SiBaSettings settings = SiBaSettings.GetInstance();
            if (settings.storage == null)
            {
                _storage = new FileStorage();
                await _storage.Load();
                Tracking.GetInstance().Track("Open", "Main");
                SiBaSettings.GetInstance().Init(_storage, "windows");
                SiBaSettings.GetInstance().SummaryCutoff = 40;
                settings.InitDone = true;
            }
            _storage = (FileStorage)SiBaSettings.GetInstance().storage;
            SiBaArticles articles = SiBaArticles.GetInstance();
            SiBaArticles.OnDataLoaded loaded = new SiBaArticles.OnDataLoaded(OnArticlesLoaded);
            articles.AddOnFinishedCallback(loaded);

            SiBaCategories.GetInstance().Load();
            articles.Load();
            this.NavigationCacheMode = NavigationCacheMode.Required;
            SystemNavigationManager.GetForCurrentView().BackRequested += HardwareButtonsNeu_BackPressed;
            //initNotifications();
        }
    }
}

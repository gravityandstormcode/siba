﻿using SiBa.UWP;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices.WindowsRuntime;
using Windows.Foundation;
using Windows.Foundation.Collections;
using Windows.Graphics.Display;
using Windows.UI.ViewManagement;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Controls;
using Windows.UI.Xaml.Controls.Primitives;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Input;
using Windows.UI.Xaml.Media;
using Windows.UI.Xaml.Navigation;
using SiBa;
using System.Threading.Tasks;


// The Basic Page item template is documented at http://go.microsoft.com/fwlink/?LinkID=390556

namespace SiBa.UWP
{
    /// <summary>
    /// An empty page that can be used on its own or navigated to within a Frame.
    /// </summary>
    public sealed partial class SettingsPage : Page
    {
        private NavigationHelper navigationHelper;
        private ObservableDictionary defaultViewModel = new ObservableDictionary();
        private Boolean init = false;


        public SettingsPage()
        {
            this.InitializeComponent();

            this.navigationHelper = new NavigationHelper(this);
            this.navigationHelper.LoadState += this.NavigationHelper_LoadState;
            this.navigationHelper.SaveState += this.NavigationHelper_SaveState;
        }

        /// <summary>
        /// Gets the <see cref="NavigationHelper"/> associated with this <see cref="Page"/>.
        /// </summary>
        public NavigationHelper NavigationHelper
        {
            get { return this.navigationHelper; }
        }

        /// <summary>
        /// Gets the view model for this <see cref="Page"/>.
        /// This can be changed to a strongly typed view model.
        /// </summary>
        public ObservableDictionary DefaultViewModel
        {
            get { return this.defaultViewModel; }
        }

        /// <summary>
        /// Populates the page with content passed during navigation.  Any saved state is also
        /// provided when recreating a page from a prior session.
        /// </summary>
        /// <param name="sender">
        /// The source of the event; typically <see cref="NavigationHelper"/>
        /// </param>
        /// <param name="e">Event data that provides both the navigation parameter passed to
        /// <see cref="Frame.Navigate(Type, Object)"/> when this page was initially requested and
        /// a dictionary of state preserved by this page during an earlier
        /// session.  The state will be null the first time a page is visited.</param>
        private void NavigationHelper_LoadState(object sender, LoadStateEventArgs e)
        {
            if (App.Filter == true)
            {
                SiBaSettings settings = SiBaSettings.GetInstance();
                danger_0.IsOn = settings.GetFilterDangerState(0);
                danger_1.IsOn = settings.GetFilterDangerState(1);
                danger_2.IsOn = settings.GetFilterDangerState(2);
            }
            else
            {
                SiBaSettings settings = SiBaSettings.GetInstance();
                danger_0.IsOn = settings.GetDangerState(0);
                danger_1.IsOn = settings.GetDangerState(1);
                danger_2.IsOn = settings.GetDangerState(2);

            }
        }

        /// <summary>
        /// Preserves state associated with this page in case the application is suspended or the
        /// page is discarded from the navigation cache.  Values must conform to the serialization
        /// requirements of <see cref="SuspensionManager.SessionState"/>.
        /// </summary>
        /// <param name="sender">The source of the event; typically <see cref="NavigationHelper"/></param>
        /// <param name="e">Event data that provides an empty dictionary to be populated with
        /// serializable state.</param>
        private void NavigationHelper_SaveState(object sender, SaveStateEventArgs e)
        {
        }

        #region NavigationHelper registration

        /// <summary>
        /// The methods provided in this section are simply used to allow
        /// NavigationHelper to respond to the page's navigation methods.
        /// <para>
        /// Page specific logic should be placed in event handlers for the  
        /// <see cref="NavigationHelper.LoadState"/>
        /// and <see cref="NavigationHelper.SaveState"/>.
        /// The navigation parameter is available in the LoadState method 
        /// in addition to page state preserved during an earlier session.
        /// </para>
        /// </summary>
        /// <param name="e">Provides data for navigation methods and event
        /// handlers that cannot cancel the navigation request.</param>
        protected override void OnNavigatedTo(NavigationEventArgs e)
        {
            if (App.Filter == true)
            {
                subline.Text = "Hier können Sie die angezeigten SiBa-Meldungen filtern. Hier gemachte Einstellungen haben keinen Einfluss darauf, welche Push-Benachrichtigungen Sie erhalten.";
            }
            else
            {
                subline.Text = "Hier können Sie einstellen, für welche SiBa-Meldungen Sie eine Push-Benachrichtigung erhalten. Hier gemachte Einstellungen haben keinen Einfluss darauf, welche SiBa-Meldungen Ihnen in der App angezeigt werden.";
            }

            this.navigationHelper.OnNavigatedTo(e);

            List<SiBaCategory> categories = SiBaCategories.GetInstance().Categories;
            if (categories != null)
            {
                for (int i = 0; i < categories.Count; i++)
                {
                    if (App.Filter == true)
                    {
                        categories[i].State = SiBaSettings.GetInstance().GetFilterCategoryState(categories[i].Id);
                    }
                    else
                    {
                        categories[i].State = SiBaSettings.GetInstance().GetCategoryState(categories[i].Id);
                    }
                }
            }

            this.Categories.ItemsSource = categories;
            SiBaCategories.OnDataLoaded loaded = new SiBaCategories.OnDataLoaded(OnCategoriesLoaded);
            Tracking.GetInstance().Track("Open", "Categories");

            Task.Run(async () =>
            {
                await Task.Delay(200);
                this.init = true;
            });

        }

        protected override void OnNavigatedFrom(NavigationEventArgs e)
        {
            this.navigationHelper.OnNavigatedFrom(e);
            SiBaSettings.GetInstance().Flush();
        }

        public void OnCategoriesLoaded(List<SiBaCategory> categories)
        {
            if (categories != null)
            {
                for (int i = 0; i < categories.Count; i++)
                {
                    if (App.Filter == true)
                    {
                        categories[i].State = SiBaSettings.GetInstance().GetFilterCategoryState(categories[i].Id);
                    }
                    else
                    {
                        categories[i].State = SiBaSettings.GetInstance().GetCategoryState(categories[i].Id);
                    }
                }
            }
            this.Categories.ItemsSource = categories;
        }

        #endregion

        private void CategoryChanged(object sender, RoutedEventArgs e)
        {
            if (this.init == false) { return; }
            ToggleSwitch switchBtn = (ToggleSwitch)sender;
            if (switchBtn != null)
            {
                SiBaCategory category = (SiBaCategory)switchBtn.DataContext;
                if (category != null)
                {
                    if (App.Filter == true)
                    {
                        SiBaSettings.GetInstance().SetFilterCategoryStage(category.Id, switchBtn.IsOn);
                        SiBaSettings.GetInstance().Flush();
                    }
                    else
                    {
                        SiBaSettings.GetInstance().SetCategoryState(category.Id, switchBtn.IsOn);
                    }
                }
            }
            ((App)App.Current).UpdatePushSettings();
        }

        private void DangerChanged(object sender, RoutedEventArgs e)
        {
            ToggleSwitch switchBtn = (ToggleSwitch)sender;
            if (switchBtn != null)
            {
                SiBaSettings settings = SiBaSettings.GetInstance();
                if (App.Filter == true)
                {
                    // App Filter settings
                    settings.SetFilterDangerState(int.Parse(switchBtn.Tag.ToString()), switchBtn.IsOn);
                }
                else {
                    // Push notification settings
                    settings.SetDangerState(int.Parse(switchBtn.Tag.ToString()), switchBtn.IsOn);
                    ((App)App.Current).UpdatePushSettings();
                }
            }
        }

        private void Partner_Tapped(object sender, TappedRoutedEventArgs e)
        {
//            Frame.Navigate(typeof(PartnerPage));
        }

        private void CloseButton_Click(object sender, RoutedEventArgs e)
        {

            System.Runtime.InteropServices.Architecture system = System.Runtime.InteropServices.RuntimeInformation.ProcessArchitecture;
            if (system == System.Runtime.InteropServices.Architecture.Arm || system == System.Runtime.InteropServices.Architecture.Arm64)
            {
                Frame.Navigate(typeof(MainPageMobile), ((FrameworkElement)sender).Tag);
            }
            else
            {
                Frame.Navigate(typeof(MainPage), ((FrameworkElement)sender).Tag);
            }

        }
    }
}

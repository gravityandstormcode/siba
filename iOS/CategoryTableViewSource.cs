﻿
using System;

using Foundation;
using UIKit;

namespace SiBa.iOS
{
	public class CategoryTableViewSource : UITableViewSource
	{
        protected bool isFilter = false;
		public CategoryTableViewSource (bool isFilter)
		{
            this.isFilter = isFilter;
		}

		public override nint NumberOfSections (UITableView tableView)
		{
			return 1;
		}

		public override nint RowsInSection (UITableView tableview, nint section)
		{
			SiBaCategories categories = SiBaCategories.GetInstance();
			if (categories == null) return 0;
			if (categories.Categories == null) return 0;
			return categories.Categories.Count;
		}

		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			SiBaCategory category = SiBaCategories.GetInstance().Categories[indexPath.Row];

			var cell = tableView.DequeueReusableCell (CategoryTableViewCell.Key) as CategoryTableViewCell;
			if (cell == null)
            {
                cell = new CategoryTableViewCell();
            }

            cell.TextLabel.Text = category.Title;
			cell.SwitchBtn.On = this.isFilter ? SiBaSettings.GetInstance().GetFilterCategoryState(category.Id) : SiBaSettings.GetInstance().GetCategoryState(category.Id);
			cell.SwitchBtn.ValueChanged += (object sender, EventArgs e) => {
                SiBaSettings settings = SiBaSettings.GetInstance();
                category.State = (sender as UISwitch).On;
                if (isFilter)
                {
                    SiBaSettings.GetInstance().SetFilterCategoryStage(category.Id, category.State);
                    settings.Flush();
                }
                else
                {
                    SiBaSettings.GetInstance().SetCategoryState(category.Id, category.State);
                    settings.Flush();
                    ((AppDelegate)UIApplication.SharedApplication.Delegate).UpdatePushSettings();
                }

                };
			return cell;
		}
	}
}


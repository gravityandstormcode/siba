﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;
namespace SiBa.iOS
{
	public class CategoryTableViewController : UITableViewController
	{
        bool isFilter = false;
		public CategoryTableViewController (bool isFilter) : base (UITableViewStyle.Grouped)
		{
            this.isFilter = isFilter;
		}

		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewWillDisappear(bool animated){
			SiBaSettings.GetInstance().Flush();
			base.ViewWillDisappear(animated);
            this.NavigationItem.Title = "Zurück";
        }

        public override void ViewWillAppear(bool animated)
        {
            TableView.SeparatorStyle = UITableViewCellSeparatorStyle.None;

            if (isFilter)
            {
                Title = "Filter";
            }
            else
            {
                Title = "Notifikationen";
            }

            // Register the TableView's data source
            TableView.Source = new CategoryTableViewSource(isFilter);
            CGRect bounds = View.Bounds;

            int yoffset = isFilter ? 128 : 160;

            const int dangerHeight = 40;
            int height = yoffset + 38 + dangerHeight * 3 + 30;
            CGRect newBounds = new CGRect(bounds.X + height, bounds.Y, bounds.Width, bounds.Height - height);
            TableView.Bounds = newBounds;

            // add header programatically
            int headerWidth = (int)bounds.Width - 32;
            UIView header = new UIView(new CGRect(8, 0, headerWidth, height));

            UILabel subheading = new UILabel(new CGRect(8, 8, headerWidth, yoffset));
            subheading.Font = UIFont.PreferredBody;
            subheading.Lines = 0;
            subheading.LineBreakMode = UILineBreakMode.WordWrap;
            if (isFilter)
            {
                subheading.Text = "Hier können Sie die angezeigten SiBa-Meldungen filtern. Hier gemachte Einstellungen haben keinen Einfluss darauf, welche Push-Benachrichtigungen Sie erhalten.";
            }
            else
            {
                subheading.Text = "Hier können Sie einstellen, für welche SiBa-Meldungen Sie eine Push-Benachrichtigung erhalten. Hier gemachte Einstellungen haben keinen Einfluss darauf, welche SiBa-Meldungen Ihnen in der App angezeigt werden.";
            }


            UILabel lightsHeading = new UILabel(new CGRect(8, yoffset, headerWidth, 40));
            lightsHeading.Font = UIFont.PreferredHeadline;
            lightsHeading.Text = isFilter ? "Risiko" : "Push-Notifikationen";

            UISwitch danger_2 = new UISwitch(new CGRect(16, yoffset + 38, 50, 30));
            UISwitch danger_1 = new UISwitch(new CGRect(16, yoffset + 38 + dangerHeight, 50, 30));
            UISwitch danger_0 = new UISwitch(new CGRect(16, yoffset + 38 + dangerHeight * 2, 50, 30));

            int switchOffset = 16 + 50 + 8;
            UILabel danger_2_text = new UILabel(new CGRect(switchOffset, yoffset + 38, headerWidth - switchOffset, 30));
            UILabel danger_1_text = new UILabel(new CGRect(switchOffset, yoffset + 38 + dangerHeight, headerWidth - switchOffset, 30));
            UILabel danger_0_text = new UILabel(new CGRect(switchOffset, yoffset + 38 + dangerHeight * 2, headerWidth - switchOffset, 30));

            SiBaSettings settings = SiBaSettings.GetInstance();


            danger_2_text.Text = "Hohes Risiko";
            danger_2.TouchUpInside += (object sender, EventArgs e) => {
                this.ToggleState(2, (UISwitch)sender);
            };

            danger_1_text.Text = "Mittleres Risiko";
            danger_1.TouchUpInside += (object sender, EventArgs e) => {
                this.ToggleState(1, (UISwitch)sender);
            };

            danger_0_text.Text = "Geringes Risiko";
            danger_0.TouchUpInside += (object sender, EventArgs e) => {
                this.ToggleState(0, (UISwitch)sender);
            };

            danger_0.SetState(this.isFilter ? settings.GetFilterDangerState(0) : settings.GetDangerState(0), false);
            danger_1.SetState(this.isFilter ? settings.GetFilterDangerState(1) : settings.GetDangerState(1), false);
            danger_2.SetState(this.isFilter ? settings.GetFilterDangerState(2) : settings.GetDangerState(2), false);

            UILabel categoriesHeading = new UILabel(new CGRect(8, yoffset + 38 + dangerHeight * 3, headerWidth, 30));
            categoriesHeading.Font = UIFont.PreferredHeadline;
            categoriesHeading.Text = "Lebensbereiche";

            header.AddSubview(lightsHeading);
            header.AddSubview(subheading);
            header.AddSubview(categoriesHeading);
            header.AddSubview(danger_0);
            header.AddSubview(danger_1);
            header.AddSubview(danger_2);
            header.AddSubview(danger_0_text);
            header.AddSubview(danger_1_text);
            header.AddSubview(danger_2_text);
            TableView.TableHeaderView = header;


            // add footer programatically
            if (!this.isFilter)
            {
                UIView footer = new UIView(new CGRect(8, 0, headerWidth, height));

                UILabel linkHeading = new UILabel(new CGRect(0, 0, headerWidth, 30));
                linkHeading.Font = UIFont.PreferredHeadline;
                linkHeading.Text = "Links";

                switchOffset = 50 + 8;
                UIButton link_0 = new UIButton(new CGRect(0, dangerHeight, headerWidth + 15, 60));
                link_0.TintColor = UIColor.FromRGB(73, 217, 41);
                link_0.BackgroundColor = UIColor.FromRGB(73, 217, 41);
                link_0.SetTitleColor(UIColor.White, UIControlState.Normal);
                NSAttributedString title = new NSAttributedString(@"Impressum und Datenschutz", null, UIColor.White, null, null, null, (NSLigatureType)1, 0, NSUnderlineStyle.None);
                link_0.SetTitle(@"Impressum und Datenschutz", UIControlState.Normal);
                link_0.SetTitleColor(UIColor.Black, UIControlState.Normal);
                link_0.SetAttributedTitle(title, UIControlState.Normal);
                link_0.HorizontalAlignment = UIControlContentHorizontalAlignment.Center;
                link_0.TouchUpInside += (object sender, EventArgs e) => {
                    ((AppDelegate)UIApplication.SharedApplication.Delegate).OpenImprintAndPrivacy();
                };

                link_0.LineBreakMode = UILineBreakMode.WordWrap;

                //link_0.ContentEdgeInsets = new UIEdgeInsets(15.0f, 15.0f, 15.0f, 15.0f);

                footer.AddSubview(linkHeading);
                footer.AddSubview(link_0);
                TableView.TableFooterView = footer;

                UIBarButtonItem Icon1 = new UIBarButtonItem(UIImage.FromFile("info.png"),
                    UIBarButtonItemStyle.Plain,
                    (sender, args) => {
                        InvokeOnMainThread(delegate {
                            ((AppDelegate)UIApplication.SharedApplication.Delegate).OpenPartners();
                        });
                    });

                UIBarButtonItem Icon2 = new UIBarButtonItem(UIImage.FromFile("contact.png"),
                    UIBarButtonItemStyle.Plain,
                    (sender, args) => {
                        InvokeOnMainThread(delegate {
                            ((AppDelegate)UIApplication.SharedApplication.Delegate).OpenShare();
                        });
                    });
                UIBarButtonItem[] Icons = { Icon1, Icon2 };
                this.NavigationItem.SetRightBarButtonItems(Icons, true);
            }
        }

        private void ToggleState(int state, UISwitch button){
			SiBaSettings settings = SiBaSettings.GetInstance ();
            if (this.isFilter)
            {
                settings.SetFilterDangerState(state, button.On);
                settings.Flush();

            }
            else
            {
                settings.SetDangerState(state, button.On);
                settings.Flush();
                ((AppDelegate)UIApplication.SharedApplication.Delegate).UpdatePushSettings();

            }
        }
	}
}


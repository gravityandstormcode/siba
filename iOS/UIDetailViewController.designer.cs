// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SiBa.iOS
{
    [Register ("UIDetailViewController")]
    partial class UIDetailViewController
    {
        [Outlet]
        UIKit.UILabel body { get; set; }


        [Outlet]
        UIKit.UIImageView danger { get; set; }


        [Outlet]
        UIKit.UILabel date { get; set; }


        [Outlet]
        UIKit.UILabel detailTitle { get; set; }


        [Outlet]
        UIKit.UIView innerFrame { get; set; }


        [Outlet]
        UIKit.UIScrollView scrollView { get; set; }

        void ReleaseDesignerOutlets ()
        {
        }
    }
}
﻿using System;
using System.IO;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.Collections;


namespace SiBa.iOS
{
	public class FileStorage : IFileStorage
	{
		public FileStorage ()
		{
			Init();
		}

		private void Init(){
			string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
			string filename = Path.Combine(documents, "..", "Library", "Settings.json");
			try
			{
				string file = File.ReadAllText(filename);
				if (file != null){
					_cache = JsonConvert.DeserializeObject<Data>(file); // comment line for debugging to get empty setings
					// handle upgrading from an older version 
					if (_cache.DangerState == null || _cache.DangerState.Count == 0){
						_cache = new Data();
					}
				}
			}
			catch (Exception e)
			{
				System.Diagnostics.Debug.WriteLine(e.ToString());
				_cache = new Data();
			}

		}

        public Boolean cacheOK()
        {
            // check if any of the needed objects was not filled with data from the cache. If so, init them now.
            if (_cache.Categories == null)
            {
                return false;
            }
            if (_cache.Articles == null)
            {
                return false;
            }
            if (_cache.CategoryState == null)
            {
                return false;
            }
            if (_cache.DangerState == null)
            {
                return false;
            }
            else
            {
                if (_cache.DangerState.Count < 1)
                {
                    return false;
                }
            }

            return true;
        }


        override public void Flush (){
			try
			{
				if (_isDirty)
				{
					string documents = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
					string filename = Path.Combine(documents, "..", "Library", "Settings.json");
					string output = JsonConvert.SerializeObject(_cache);
					File.WriteAllText(filename, output);
					_isDirty = false;
				}
			}
			catch (Exception e)
			{
					System.Diagnostics.Debug.WriteLine(e.ToString());
				_cache = new Data();
			}
		}

	}
}


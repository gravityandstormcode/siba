﻿using System;
using UIKit;

namespace SiBa.iOS
{[Foundation.Register ("SiBaUIScrollView")]

	public partial class SiBaUIScrollView : UIScrollView
	{
		public SiBaUIScrollView (IntPtr handle) : base (handle)
		{
		}

		public override bool TouchesShouldCancelInContentView (UIView view)
		{
			return view.GetType() != typeof(UIButton);
		}
	}
}


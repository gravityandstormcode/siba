﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;
using ObjCRuntime;

namespace SiBa.iOS
{
	public class WebView : UIViewController
	{
		private class SiBaWebViewDelegate : UIWebViewDelegate
		{
			// get's never called:
			public override bool ShouldStartLoad (UIWebView webView, NSUrlRequest request, UIWebViewNavigationType navigationType)
			{
				return true;
			}
		};

		private UIWebView _webView;
		private UINavigationController _navigationController;
		private UIActivityIndicatorView _activityIndicatorView;
		private UIView _blankOverlay;
		private UIColor _origBarColor;

		public WebView (UINavigationController navigationController) : base ("WebView", null)
		{
			_navigationController = navigationController;
			CGRect bounds = _navigationController.View.Bounds;

			_webView = new UIWebView (bounds);
			_webView.ScalesPageToFit = true;
			View.AddSubview (_webView);

			_blankOverlay = new UIView (bounds);
			_blankOverlay.BackgroundColor = new UIColor (1, 1, 1, 1.0f);

			_activityIndicatorView = new UIActivityIndicatorView (UIActivityIndicatorViewStyle.Gray);
			_activityIndicatorView.Center = _blankOverlay.Center;
			_activityIndicatorView.StartAnimating ();
			_blankOverlay.AddSubview (_activityIndicatorView);
//			_webView.Delegate = new SiBaWebViewDelegate();
		}



		public override void DidReceiveMemoryWarning ()
		{
			// Releases the view if it doesn't have a superview.
			base.DidReceiveMemoryWarning ();
			
			// Release any cached data, images, etc that aren't in use.
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
		}

		public override void ViewWillDisappear(bool animated){
			if (_navigationController != null) {
				_navigationController.NavigationBar.BarTintColor = _origBarColor;
			}
			base.ViewWillDisappear(animated);
		}

		public void Load(Link link, int dangerLevel){
			Title = link.Title;

			UIColor color = null;
			switch(dangerLevel){
			case 0:
				color = new UIColor (0, 0.545f, 0.424f, 1);
				break;
			case 1:
				color = new UIColor (0.863f, 0.733f, 0.278f, 1);
				break;
			case 2:
				color = new UIColor (0.953f, 0.345f, 0.345f, 1);				
				break;
			}

			_webView.LoadRequest (new NSUrlRequest (new NSUrl(link.Url)));
			_webView.LoadFinished += (sender, args) => {
				_blankOverlay.RemoveFromSuperview();
			};
			_blankOverlay.BackgroundColor = new UIColor (1, 1, 1, 1);
			_webView.AddSubview (_blankOverlay);

			if (_navigationController != null){
				_origBarColor = _navigationController.NavigationBar.BarTintColor;
				_navigationController.NavigationBar.BarTintColor = color;
				_navigationController.PushViewController (this, true);
			}
		}

	}
}


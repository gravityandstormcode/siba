// WARNING
//
// This file has been generated automatically by Visual Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SiBa.iOS
{
    [Register ("PartnerViewController")]
    partial class PartnerViewController
    {
        [Outlet]
        UIKit.UIButton button_bankenverband { get; set; }


        [Outlet]
        UIKit.UIButton button_bka { get; set; }


        [Outlet]
        UIKit.UIButton button_bsi { get; set; }


        [Outlet]
        UIKit.UIButton button_bundesdruckerei { get; set; }


        [Outlet]
        UIKit.UIButton button_dsin { get; set; }


        [Outlet]
        UIKit.UIButton button_eco { get; set; }


        [Outlet]
        UIKit.UIButton button_fireeye { get; set; }


        [Outlet]
        UIKit.UIButton button_gdv { get; set; }


        [Outlet]
        UIKit.UIButton button_microsoft { get; set; }


        [Outlet]
        UIKit.UIButton button_nokia { get; set; }


        [Outlet]
        UIKit.UIButton button_telekom { get; set; }


        [Outlet]
        UIKit.UIButton button_polizei { get; set; }

        [Outlet]
        UIKit.UIButton button_avira { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UIButton button_bmi { get; set; }


        [Action ("bka_clicked:")]
        partial void bka_clicked (Foundation.NSObject sender);


        [Action ("telekom_clicked:")]
        partial void telekom_clicked (Foundation.NSObject sender);


        [Action ("gdv_clicked:")]
        partial void gdv_clicked (Foundation.NSObject sender);


        [Action ("bundesdruckerei_clicked:")]
        partial void bundesdruckerei_clicked (Foundation.NSObject sender);


        [Action ("bsi_clicked:")]
        partial void bsi_clicked (Foundation.NSObject sender);


        [Action ("nokia_clicked:")]
        partial void nokia_clicked (Foundation.NSObject sender);


        [Action ("microsoft_clicked:")]
        partial void microsoft_clicked (Foundation.NSObject sender);


        [Action ("bankenverband_clicked:")]
        partial void bankenverband_clicked (Foundation.NSObject sender);


        [Action ("dsin_clicked:")]
        partial void dsin_clicked (Foundation.NSObject sender);


        [Action ("fireeye_clicked:")]
        partial void fireeye_clicked (Foundation.NSObject sender);


        [Action ("eco_clicked:")]
        partial void eco_clicked (Foundation.NSObject sender);


        [Action ("polizei_clicked:")]
        partial void polizei_clicked (Foundation.NSObject sender);

        [Action ("avira_clicked:")]
        partial void avira_clicked (Foundation.NSObject sender);

        [Action ("Button_bmi_TouchUpInside:")]
        [GeneratedCode ("iOS Designer", "1.0")]
        partial void Button_bmi_TouchUpInside (UIKit.UIButton sender);

        void ReleaseDesignerOutlets ()
        {
            if (button_bmi != null) {
                button_bmi.Dispose ();
                button_bmi = null;
            }
        }
    }
}
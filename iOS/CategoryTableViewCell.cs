﻿
using System;

using Foundation;
using UIKit;
using CoreGraphics;

namespace SiBa.iOS
{
	public class CategoryTableViewCell : UITableViewCell
	{
		public static readonly NSString Key = new NSString ("CategoryTableViewCell");
		public UISwitch SwitchBtn;

		public CategoryTableViewCell () : base (UITableViewCellStyle.Default, Key)
		{
			SwitchBtn = new UISwitch(new CGRect(8,8,50,Bounds.Height));
			this.ContentView.AddSubview (SwitchBtn);
			BackgroundColor = UIColor.GroupTableViewBackgroundColor;

		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
			TextLabel.Frame = new CGRect(50+16,0,Bounds.Width-58-16,Bounds.Height);
		}
	}
}


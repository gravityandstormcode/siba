﻿using System;
using UIKit;
using Foundation;
using CoreGraphics;
using System.Net.Http;
using System.Threading.Tasks;
using System.Text.RegularExpressions;

namespace SiBa.iOS
{
	public partial class ShareViewController : UIViewController
	{
		public ShareViewController(IntPtr handle) : base (handle)
		{
		}

        static float KeyboardHeight = 0.0f;

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();

            Foundation.NSNotificationCenter.DefaultCenter.AddObserver(new NSString("UIDeviceOrientationDidChangeNotification"), DeviceRotated);
            Title = "Nachricht an DsiN";

            NSObject notification = UIKeyboard.Notifications.ObserveDidChangeFrame((sender, args) => {
                if (args.FrameBegin.Y > args.FrameEnd.Y)
                {
                    KeyboardHeight = (float)args.FrameEnd.Size.Height;
                    UpdateFrame();
                }

                else
                {
                }

            });
        }

        void DeviceRotated(NSNotification notification)
        {
            UpdateFrame();
            System.Diagnostics.Debug.WriteLine(UIDevice.CurrentDevice.Orientation);
        }

        public void UpdateFrame()
        {
            // If we are in landscape mode we strech the innerView ...
            CGSize viewSize = this.View.Frame.Size;
            if (theScrollView.Frame.Height > theScrollView.Frame.Width)
            {
                innerView.Frame = new CGRect(0, 0, viewSize.Width, viewSize.Height);
            }
            else
            {
                innerView.Frame = new CGRect(0, 0, viewSize.Width, (viewSize.Height * 2));
            }
            //KeyboardHeight
            CGSize contenSize = new CGSize(innerView.Frame.Size.Width, innerView.Frame.Size.Height + KeyboardHeight);
            theScrollView.ContentSize = contenSize;
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            UpdateFrame();
            UINavigationController navigationController = ((AppDelegate)UIApplication.SharedApplication.Delegate).Window.RootViewController as UINavigationController;
            navigationController.NavigationBar.BarTintColor = null;

        }

        async partial void SendButton_TouchUpInside(UIButton sender)
        {
            string from = From.Text;
            if (from == "") { from = "benutzerhatkeineadresseangegeben@siba.com"; }

            if (Regex.Match(from, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
            {

            }
            else
            {
                //Create Alert
                var okAlertController = UIAlertController.Create("Fehler", "Die eigegebene Emailadresse scheint ungültig zu sein.", UIAlertControllerStyle.Alert);

                //Add Action
                okAlertController.AddAction(UIAlertAction.Create("Weiter", UIAlertActionStyle.Default, null));

                // Present Alert
                PresentViewController(okAlertController, true, null);
                return;
            }


            bool isAllEmpty = true;


            string title = Subject.Text;
            if (title == "") { title = "Benutzer hat keinen Titel angegeben."; } else { isAllEmpty = false; }

            string link = Link.Text;
            if (link == "") { link = "Benutzer hat keinen Link angegeben."; } else { isAllEmpty = false; }

            string body = Message.Text;
            if (body == "") { body = "Benutzer hat kein Nachricht angegeben."; } else { isAllEmpty = false; }

            string code = SiBa.Keys.SendGridCode;
            string url = SiBa.Keys.SendGridURL;
            string to = "siba@dsin.de";

            if (!isAllEmpty)
            {
                string RestUrl = url + "code=" + code + "&from=" + from + "&to=" + to + "&link=" + link + "&title=" + title + "&message=" + body;
                sendEmailAndWait(RestUrl);
            }
        }

        async void sendEmailAndWait(string url) {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            ShowAlert();
        }

        async void ShowAlert() {
            var result = await ShowAlert("Vielen Dank für Ihre Nachricht.", "Die Daten wurden an Deutschland sicher im Netz weitergeleitet.", "OK");
            UINavigationController navigationController = ((AppDelegate)UIApplication.SharedApplication.Delegate).Window.RootViewController as UINavigationController;
            navigationController.PopViewController(true);
        }

        // Displays a UIAlertView and returns the index of the button pressed.
        public static Task<int> ShowAlert(string title,
                                           string message,
                                           params string[] buttons)
        {
            var tcs = new TaskCompletionSource<int>();
            var alert = new UIAlertView
            {
                Title = title,
                Message = message
            };
            foreach (var button in buttons)
                alert.AddButton(button);
            alert.Clicked += (s, e) => tcs.TrySetResult((int)e.ButtonIndex);
            alert.Show();
            return tcs.Task;
        }




    }
}


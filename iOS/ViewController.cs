using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using System.Collections.Generic;

namespace SiBa.iOS
{
	partial class ViewController : UIViewController
    {
        //		static NSString MyCellId = new NSString ("MainTableCell");
        private bool _articlesLoaded = false;

		public ViewController (IntPtr handle) : base (handle)
		{
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			TableView.EstimatedRowHeight = 200;
			TableView.RowHeight = UITableView.AutomaticDimension;
            //			TableView.RegisterClassForCellReuse (typeof(MainTableCell), MyCellId);


            var buttons =new UIBarButtonItem[] {
            new UIBarButtonItem(UIImage.FromFile("burger-menu.png"),
                UIBarButtonItemStyle.Plain,
                (sender, args) =>
                {
                    InvokeOnMainThread(delegate
                    {
                        ((AppDelegate)UIApplication.SharedApplication.Delegate).OpenFilters();
                    });
                }), 
            new UIBarButtonItem(UIImage.FromFile("filter-menu.png"),
                UIBarButtonItemStyle.Plain,
                (sender, args) => {
                    InvokeOnMainThread(delegate {
                        ((AppDelegate)UIApplication.SharedApplication.Delegate).OpenCategories();
                    });
                })
            };

            this.NavigationItem.SetRightBarButtonItems( buttons, true);

			TableView.Source = new MainTableSource();

			SiBaArticles.OnDataLoaded loaded = new SiBaArticles.OnDataLoaded (OnNewsLoadedEvent);
			SiBaArticles articles = SiBaArticles.GetInstance ();
			articles.AddOnFinishedCallback (loaded);
            if (this._articlesLoaded == false)
            {
                ((AppDelegate)UIApplication.SharedApplication.Delegate).ShowActivityIndicator(true);
            }

		}

        public void UpdateArticles()
        {
            ((MainTableSource)TableView.Source).Filter.Filter();
            TableView.ReloadData();
        }

        public override void ViewWillAppear(bool animated){
			base.ViewWillAppear (animated);
			this.NavigationItem.Title = "SiBa";
			SiBaSettings.GetInstance().SendSettings();
			Tracking.GetInstance().Track("Open","Main");
            this.UpdateArticles();
		}
		public override void ViewWillDisappear (bool animated)
		{
			base.ViewWillDisappear (animated);
			this.NavigationItem.Title = "Zurück";
		}


		public void OnNewsLoadedEvent(List<SiBaArticle> newArticles){
			InvokeOnMainThread(delegate{
				UpdateArticles();
			});
			((AppDelegate)UIApplication.SharedApplication.Delegate).ShowActivityIndicator(false);
            this._articlesLoaded = true;

        }

	}
}

// This file has been autogenerated from a class added in the UI designer.

using System;

using Foundation;
using UIKit;
using CoreGraphics;
using System.Collections.Generic;

namespace SiBa.iOS
{
	public partial class MainTableCell : UITableViewCell
	{
		private SiBaArticle _article;
		private ViewHelper _viewHelper;

		public MainTableCell (IntPtr handle) : base (handle)
		{
		}


		public SiBaArticle Article{
			set{
				_article = value;	

				if (_viewHelper != null){
					_viewHelper.RemoveFromView ();
				}
				_viewHelper = new ViewHelper (this.innerFrame, this.body, value);
				_viewHelper.AddToView ();

				this.UpdateConstraints ();
				this.SetNeedsLayout ();
				this.LayoutIfNeeded ();
			}
		}

		public override void LayoutSubviews(){
			var titleAttr = new UIStringAttributes {						
				Font = UIFont.SystemFontOfSize(15),
			};

			if (_article.Body.Length > _article.Summary.Length && UIDevice.CurrentDevice.UserInterfaceIdiom == UIUserInterfaceIdiom.Phone){
				
				const string readMore = "... Weiterlesen";
				int summaryShort = _article.Summary.Length - readMore.Length;

				var readModeAttr = new UIStringAttributes {						
					Font = UIFont.SystemFontOfSize(15),
					ForegroundColor = new UIColor(0.7f,0.7f,0.7f,1)
				};

				NSMutableAttributedString previewAttr = new NSMutableAttributedString (_article.Summary);
				previewAttr.SetAttributes (titleAttr,new NSRange(0,summaryShort));
				previewAttr.SetAttributes (readModeAttr, new NSRange (summaryShort, readMore.Length));
				this.body.AttributedText = previewAttr;

			}else{
				NSMutableAttributedString bodyText = new NSMutableAttributedString (_article.Body);
				bodyText.SetAttributes (titleAttr,new NSRange(0,bodyText.Length));
				this.body.AttributedText = bodyText;
			}

			UITapGestureRecognizer labelTap = new UITapGestureRecognizer(() => {
				InvokeOnMainThread(delegate{
					((AppDelegate)UIApplication.SharedApplication.Delegate).OpenDetails(_article);
				});
			});

			this.innerFrame.AddGestureRecognizer (labelTap);
			this.title.Text = _article.Title.ToUpper();
			this.date.Text = _article.Age;
			this.danger.Image = UIImage.FromBundle("danger-"+_article.Level+".png");
		}

		public void refreshAllConstraints () {
			refreshConstraints(this.title);
			refreshConstraints(this.innerFrame);
			refreshConstraints(this.body);
			refreshConstraints(this.danger);
			refreshConstraints(this.Superview);
		}

		public void refreshConstraints(UIView theView) {
			if (theView == null) {
				return;
			}
			NSLayoutConstraint[] constraints;
			constraints = theView.Constraints;

			theView.RemoveConstraints(constraints);
			theView.SetNeedsLayout();
			theView.SetNeedsDisplay();
			theView.LayoutSubviews();
			theView.AddConstraints(constraints);
			theView.SetNeedsLayout();
			theView.SetNeedsDisplay();
			theView.LayoutSubviews();
		}
	}
}

﻿using Foundation;
using UIKit;
using SiBa;
using System.Collections.Generic;
using Plugin.Share;
using WindowsAzure.Messaging;
using System;
using Newtonsoft.Json;
using System.Threading.Tasks;
using UserNotifications;
using CoreGraphics;

namespace SiBa.iOS
{
	// The UIApplicationDelegate for the application. This class is responsible for launching the
	// User Interface of the application, as well as listening (and optionally responding) to application events from iOS.
	[Register ("AppDelegate")]
	public class AppDelegate : UIApplicationDelegate
	{
		// class-level declarations
		private WebView _webView = null;
		private FileStorage _storage = new FileStorage();
		private UIActivityIndicatorView _activityIndicatorView;
        private SBNotificationHub Hub { get; set; }
        private Boolean DidRegisterWithNotificationHub = false;

        public override UIWindow Window {
			get;
			set;
		}

		public override bool FinishedLaunching (UIApplication application, NSDictionary launchOptions)
		{
            // Override point for customization after application launch.
            // If not required for your application you can safely delete this method

            // Code to start the Xamarin Test Cloud Agent

            Task<int> InitTask = InitTasks();
            var intResult = InitTask;

            if (UIDevice.CurrentDevice.CheckSystemVersion(10, 0))
            {
                UNUserNotificationCenter.Current.RequestAuthorization(UNAuthorizationOptions.Alert | UNAuthorizationOptions.Badge | UNAuthorizationOptions.Sound,
                                                                        (granted, error) =>
                                                                        {
                                                                            if (granted)
                                                                                InvokeOnMainThread(UIApplication.SharedApplication.RegisterForRemoteNotifications);
                                                                        });
            }
            else if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
            {
                var pushSettings = UIUserNotificationSettings.GetSettingsForTypes(
                        UIUserNotificationType.Alert | UIUserNotificationType.Badge | UIUserNotificationType.Sound,
                        new NSSet());

                UIApplication.SharedApplication.RegisterUserNotificationSettings(pushSettings);
                UIApplication.SharedApplication.RegisterForRemoteNotifications();
            }
            else
            {
                UIRemoteNotificationType notificationTypes = UIRemoteNotificationType.Alert | UIRemoteNotificationType.Badge | UIRemoteNotificationType.Sound;
                UIApplication.SharedApplication.RegisterForRemoteNotificationTypes(notificationTypes);
            }


            return true;
		}

        public async Task<int> InitTasks()
        {

#if ENABLE_TEST_CLOUD
            Xamarin.Calabash.Start();
#endif

            UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;

            // init loading of articles and categories
            SiBaSettings.GetInstance().Init(_storage, "ios");
            if (_storage.cacheOK() == false)
            {
                SiBaSettings.GetInstance().SetArticles(null);
                SiBaSettings.GetInstance().SetCategories(null);
            }

            // SiBaCategories.GetInstance().Load();
            // SiBaArticles.GetInstance().Load();

            return 1; 
        }



        public override void OnResignActivation (UIApplication application)
		{
			// Invoked when the application is about to move from active to inactive state.
			// This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
			// or when the user quits the application and it begins the transition to the background state.
			// Games should use this method to pause the game.
		}

		public override void DidEnterBackground (UIApplication application)
		{
			// Use this method to release shared resources, save user data, invalidate timers and store the application state.
			// If your application supports background exection this method is called instead of WillTerminate when the user quits.
		}

		public override void WillEnterForeground (UIApplication application)
		{
			// Called as part of the transiton from background to active state.
			// Here you can undo many of the changes made on entering the background.
		}

		public override void OnActivated (UIApplication application)
		{
            // Restart any tasks that were paused (or not yet started) while the application was inactive. 
            // If the application was previously in the background, optionally refresh the user interface.
            var someTask = Task.Run(async () =>
            {
                SiBaArticles.GetInstance().Load();
                SiBaCategories.GetInstance().Load();
            });
			ShowActivityIndicator (true);
			UIApplication.SharedApplication.ApplicationIconBadgeNumber = 0;
		}

		public async override void WillTerminate (UIApplication application)
		{
			await SiBaSettings.GetInstance ().SendSettings ();
			_storage.Flush ();
			// Called when the application is about to terminate. Save data, if needed. See also DidEnterBackground.
			Tracking.GetInstance().Track("App Shutdown");
		}

		public override void RegisteredForRemoteNotifications (UIApplication application, NSData deviceToken)
		{
            // Get current device token
            var token = deviceToken.Description;

            // The registration of the pushtoken calls for the NSData object and not just a string.
            // So the deviceToken needs to get serialized and saved.
            var plist = NSUserDefaults.StandardUserDefaults;
            // make the deviceToken (NSData into a string
            var tokenStringBase64 = deviceToken.GetBase64EncodedString(NSDataBase64EncodingOptions.None);
            // save the string
            plist.SetString(tokenStringBase64, "pushtokendata");

            // Save the current pushtoken
            plist.SetString(token, "pushtoken");

            UpdatePushSettings();

            SiBaNotification notification = new SiBaNotification();
            notification.registerToken(token);


            // DEBUG ONLY
            /*
                        NSMutableSet tags = new NSMutableSet();
                        tags.Add(new NSString("debug_debug"));
                        if (Hub == null)
                        {
                            Hub = new SBNotificationHub(SiBa.Keys.NotificationHubEndpoint, SiBa.Keys.NotificationHubName);
                        }

                        Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) =>
                        {
                            if (errorCallback != null)
                                System.Diagnostics.Debug.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                        });
            */
            ((AppDelegate)UIApplication.SharedApplication.Delegate).UpdatePushSettings();


        }

        public void UpdatePushSettings() {
            var plist = NSUserDefaults.StandardUserDefaults;
            // Get the token from the userdefaults
            string PushToken = plist.StringForKey("pushtoken");

            // read the string
            string PushTokenFromSettings = plist.StringForKey("pushtokendata");
            if (PushTokenFromSettings == null) { return; }
            // Make the string into an NSData Object again
            var deviceToken = new NSData(PushTokenFromSettings, NSDataBase64DecodingOptions.None);

            if (Hub == null)
            {
                Hub = new SBNotificationHub(SiBa.Keys.NotificationHubEndpoint, SiBa.Keys.NotificationHubName);
            }

            if (deviceToken != null)
            {
                //Hub.UnregisterAllAsync(PushTokenFromSettings, (error) =>
                //{
                //    if (error != null)
                //    {
                //        System.Diagnostics.Debug.WriteLine("Error calling Unregister: {0}", error.ToString());
                //        return;
                //    }

                    NSMutableSet tags = new NSMutableSet();
                    SiBaSettings settings = SiBaSettings.GetInstance();

                    bool Red = settings.GetDangerState(2);
                    bool Yellow = settings.GetDangerState(1);
                    bool Green = settings.GetDangerState(0);

                    List<SiBaCategory> Categories = settings.GetCategories();
                    foreach (SiBaCategory category in Categories)
                    {
                        //   try
                        //{
                        category.State = settings.GetCategoryState(category.Id);
                        if (category.State == true)
                        {
                            if (Red == true)
                            {
                                NSString SingleTag = new NSString("red_" + category.Id.ToString());
                                tags.Add(SingleTag);
                            }
                            if (Yellow == true)
                            {
                                NSString SingleTag = new NSString("yellow_" + category.Id.ToString());
                                tags.Add(SingleTag);
                            }
                            if (Green == true)
                            {
                                NSString SingleTag = new NSString("green_" + category.Id.ToString());
                                tags.Add(SingleTag);
                            }
                        }
                        //}
                        //catch (Exception e)
                        //{

                        //}
                    }

                    Boolean DebugTag = settings.GetDebugTag();

                    if (DebugTag == true)
                    {
                        tags.Add(new NSString("debug_debug"));
                    }


                //UIAlertView alert = new UIAlertView()
                //{
                //    Title = "Push Token",
                //    Message = PushToken
                //};
                //alert.AddButton("OK");
                //alert.Show();


                // Hub = new SBNotificationHub(Constants.ListenConnectionString, Constants.NotificationHubName);

                Hub.UnregisterAll(deviceToken, (error) => {
                    if (error != null)
                    {
                        System.Diagnostics.Debug.WriteLine("Error calling Unregister: {0}", error.ToString());
                        return;
                    }
                    tags.Add(new NSString("debug_debug"));
                    Hub.RegisterNativeAsync(deviceToken, tags);

                    // Code for the older notificationhub package (-update)
                    //Hub.RegisterNativeAsync(deviceToken, tags, (errorCallback) =>
                    //{
                    //    if (errorCallback != null)
                    //        System.Diagnostics.Debug.WriteLine("RegisterNativeAsync error: " + errorCallback.ToString());
                    //});
                });
            }

        }
        public override void FailedToRegisterForRemoteNotifications (UIApplication application , NSError error)
		{
			new UIAlertView("Push Notifikation konnte nicht registriert werden", error.LocalizedDescription, null, "OK", null).Show();
		}

		public override void ReceivedRemoteNotification (UIApplication application, NSDictionary userInfo){
            ProcessNotification(userInfo, false);

            SiBaArticles.GetInstance().Load();

            //UIAlertView alert = new UIAlertView()
            //{
            //    Title = "Pushnachricht",
            //    Message = "Eine Pushnachricht ist eingetroffen."
            //};
            //alert.AddButton("OK");
            //alert.Show();

        }

        void ProcessNotification(NSDictionary options, bool fromFinishedLaunching)
        {
            // Check to see if the dictionary has the aps key.  This is the notification payload you would have sent
            if (null != options && options.ContainsKey(new NSString("aps")))
            {
                //Get the aps dictionary
                NSDictionary aps = options.ObjectForKey(new NSString("aps")) as NSDictionary;

                string alert = string.Empty;

                //Extract the alert text
                // NOTE: If you're using the simple alert by just specifying
                // "  aps:{alert:"alert msg here"}  ", this will work fine.
                // But if you're using a complex alert with Localization keys, etc.,
                // your "alert" object from the aps dictionary will be another NSDictionary.
                // Basically the JSON gets dumped right into a NSDictionary,
                // so keep that in mind.
                if (aps.ContainsKey(new NSString("alert")))
                    alert = (aps[new NSString("alert")] as NSString).ToString();

                //If this came from the ReceivedRemoteNotification while the app was running,
                // we of course need to manually process things like the sound, badge, and alert.
                if (!fromFinishedLaunching)
                {
                    //Manually show an alert
                    if (!string.IsNullOrEmpty(alert))
                    {
                        UIAlertView avAlert = new UIAlertView("Pushnachricht", alert, null, "OK", null);
                        avAlert.Show();
                    }
                }
            }
        }


        public void ShowActivityIndicator(bool show){
            InvokeOnMainThread(delegate {
                if (show)
                {
                    if (_activityIndicatorView == null)
                    {
                        _activityIndicatorView = new UIActivityIndicatorView(UIActivityIndicatorViewStyle.Gray);
                    }
                    UINavigationController navigationController = Window.RootViewController as UINavigationController;
                    UIView parent = navigationController.TopViewController.View;
                    _activityIndicatorView.Center = parent.Center;
                    _activityIndicatorView.StartAnimating();
                    parent.AddSubview(_activityIndicatorView);
                }
                else
                {
                    if (_activityIndicatorView != null)
                    {
                        _activityIndicatorView.RemoveFromSuperview();
                        _activityIndicatorView = null;
                    }
                }
            }); 
		}

		public void OpenWeb(Link link, int dangerLevel){
			Tracking.GetInstance().Track("Open Url",link.Url);
			if (_webView == null){
				UINavigationController navigationController = Window.RootViewController as UINavigationController;
				_webView = new WebView (navigationController);	
			}

			_webView.Load (link, dangerLevel);
		}

        public void OpenFilters()
        {
            Tracking.GetInstance().Track("Open", "Categories");
            UINavigationController navigationController = Window.RootViewController as UINavigationController;
            CategoryTableViewController catViewController = new CategoryTableViewController(false);
            navigationController.PushViewController(catViewController, true);
        }
        public void OpenCategories(){
            Tracking.GetInstance().Track("Open", "Filters");
            UINavigationController navigationController = Window.RootViewController as UINavigationController;
            CategoryTableViewController catViewController = new CategoryTableViewController(true);
            navigationController.PushViewController(catViewController, true);
		}

		public async void OpenShareDialog(SiBaArticle article){
			Tracking.GetInstance().Track("Open","Share");
			if (article != null) {

				var title = article.Title;
				var message = article.Body;
				var url = "https://www.sicher-im-netz.de/node/" + article.Id.ToString();

				try
				{
					var items = new NSObject[] { NSObject.FromObject(title), new NSString(url) };
					var activityController = new UIActivityViewController(items, null);
					activityController.SetValueForKey(NSObject.FromObject(title), new NSString("subject"));
				
					UINavigationController navigationController = Window.RootViewController as UINavigationController;
					UIView parent = navigationController.TopViewController.View;
                    if (Xamarin.Essentials.DeviceIdiom.Tablet == Xamarin.Essentials.DeviceInfo.Idiom)
                    {
                        activityController.PopoverPresentationController.SourceRect = new CGRect(100, 400, 100, 100);
                    }

                    if (UIDevice.CurrentDevice.CheckSystemVersion(8, 0))
					{
						if (activityController.PopoverPresentationController != null)
						{
							activityController.PopoverPresentationController.SourceView = parent;
						}
					}

					await navigationController.PresentViewControllerAsync(activityController, true);
				}
				catch (System.Exception ex)
				{
					System.Diagnostics.Debug.WriteLine("Unable to share text" + ex.Message);
				}



			}
		}

		public void OpenPartners(){
			Tracking.GetInstance().Track("Open","Partner");
			UIViewController partnerController = UIStoryboard.FromName("Main",null).InstantiateViewController("Partners");

			UINavigationController navigationController = Window.RootViewController as UINavigationController;
			navigationController.PushViewController (partnerController, true);
		}

        public void OpenShare()
        {
            UIViewController shareController = UIStoryboard.FromName("Main", null).InstantiateViewController("Share");

            UINavigationController navigationController = Window.RootViewController as UINavigationController;
            navigationController.PushViewController(shareController, true);
        }


        public void OpenImprintAndPrivacy()
		{
			Tracking.GetInstance().Track("Open", "ImprintAndPrivacy");
			UIViewController partnerController = UIStoryboard.FromName("Main", null).InstantiateViewController("ImprintAndPrivacy");

			UINavigationController navigationController = Window.RootViewController as UINavigationController;
			navigationController.PushViewController(partnerController, true);
		}



		public void OpenDetails(SiBaArticle article){
			Tracking.GetInstance ().Track ("Open", "Details", article.Title);
			UIDetailViewController detailViewController = UIStoryboard.FromName("Main",null).InstantiateViewController("Details") as UIDetailViewController;
			detailViewController.Article = article;

			UINavigationController navigationController = Window.RootViewController as UINavigationController;
			navigationController.PushViewController (detailViewController, true);
		}
	}
}



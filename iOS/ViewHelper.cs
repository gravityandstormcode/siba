﻿using System;
using System.Collections.Generic;
using UIKit;
using Foundation;

using CoreGraphics;

namespace SiBa.iOS
{
	public class ViewHelper
	{
		private List<Link> _links;
		private List<UIView> _linkElements;
		private UIView _innerFrame;
		private UIView _body;
		private SiBaArticle _article;

		public ViewHelper (UIView innerFrame, UIView body, SiBaArticle article)
		{
			_innerFrame = innerFrame;
			_body = body;
			_article = article;
		}

		public static UIColor GetDangerColor(int dangerLevel){
			switch(dangerLevel){
			case 0:
				return new UIColor (0, 0.545f, 0.424f, 1);
			case 1:
				return new UIColor (0.863f, 0.733f, 0.278f, 1);
			case 2:
				return new UIColor (0.953f, 0.345f, 0.345f, 1);				
			}
			return null;
		}

		public List<UIView> LinkElements{
			get{
				return _linkElements;
			}
		}

		public void RemoveFromView(){
			if (_linkElements != null){
				foreach(UIView link in _linkElements){
					link.RemoveFromSuperview();
				}
			}
		}

		public void AddToView(bool addBottomConstraint = true){

			if (_linkElements != null){
				foreach(UIView link in _linkElements){
					link.RemoveFromSuperview();
				}
			}

			_linkElements = new List<UIView> ();

			UIView lastUiLink = null;

            UILabel titleLabel = null;

            titleLabel = new UILabel();
            titleLabel.Text = "Wer kann mir helfen?";
            titleLabel.Font = UIFont.BoldSystemFontOfSize(15);
            titleLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _innerFrame.AddSubview(titleLabel);
            _innerFrame.AddConstraint(NSLayoutConstraint.Create(lastUiLink != null ? lastUiLink : _body, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, titleLabel, NSLayoutAttribute.Top, 1, -16));
            _innerFrame.AddConstraint(NSLayoutConstraint.Create(titleLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Left, 1, 16));
            _innerFrame.AddConstraint(NSLayoutConstraint.Create(titleLabel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Right, 1, -16));
            lastUiLink = titleLabel;
            _linkElements.Add(titleLabel);
            // ***************
            AddLinksToView(_article.url_explanation, ref lastUiLink, UIImage.FromFile("bullet.png"));


            titleLabel = new UILabel();
            titleLabel.Text = "Wie schütze ich mich?";
            titleLabel.Font = UIFont.BoldSystemFontOfSize(15);
            titleLabel.TranslatesAutoresizingMaskIntoConstraints = false;
            _innerFrame.AddSubview(titleLabel);
            _innerFrame.AddConstraint(NSLayoutConstraint.Create(lastUiLink != null ? lastUiLink : _body, NSLayoutAttribute.Bottom, NSLayoutRelation.Equal, titleLabel, NSLayoutAttribute.Top, 1, -16));
            _innerFrame.AddConstraint(NSLayoutConstraint.Create(titleLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Left, 1, 16));
            _innerFrame.AddConstraint(NSLayoutConstraint.Create(titleLabel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Right, 1, -16));
            lastUiLink = titleLabel;
            _linkElements.Add(titleLabel);

            // ***************
            AddLinksToView(_article.url_action,ref lastUiLink,UIImage.FromFile ("bullet.png"));

			if (addBottomConstraint == false) {
				AddShareButtonToView (lastUiLink);
			}

			if (addBottomConstraint){
				if (lastUiLink != null){
					_innerFrame.AddConstraint (NSLayoutConstraint.Create (_innerFrame,NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,lastUiLink,NSLayoutAttribute.Bottom,1,8));
				}else{
					// set body to bottom
					_innerFrame.AddConstraint (NSLayoutConstraint.Create (_innerFrame,NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,_body,NSLayoutAttribute.Bottom,1,8));
				}
			}

			_innerFrame.UpdateConstraints ();
		}

		private void AddLinksToView(List<Link> links, ref UIView lastUiLink, UIImage image){
			if (links != null){

				if (_links == null){
					_links = new List<Link> ();
				}

				const int titleOffset = -16;

				foreach(Link textlink in links){
					//if (textlink.Style == Link.LinkStyle.TitleLinkNoLink  && links.Count > 1) {
					//	continue;
					//}

					var linkLabel = new UILabel (new CoreGraphics.CGRect(10,10,300,30));  // will be overwritten by contraints
					linkLabel.LineBreakMode = UILineBreakMode.WordWrap;
					linkLabel.Lines = 0;

					var attr = new UIStringAttributes {						
						Font = UIFont.SystemFontOfSize(15),
						UnderlineStyle = NSUnderlineStyle.Single,
					} ;

					_links.Add (textlink);
					int index = _links.IndexOf (textlink);
					linkLabel.Tag = index;

					NSMutableAttributedString linkTitle;
					linkTitle = new NSMutableAttributedString (textlink.Title);
					linkTitle.SetAttributes(attr.Dictionary,new NSRange(0,linkTitle.Length));
					linkLabel.LineBreakMode =  UILineBreakMode.WordWrap;
					linkLabel.TextAlignment = UITextAlignment.Left;
					linkLabel.AttributedText = linkTitle;
					//					if (titleLabel != null){
					//						linkLabel.SetImage (image,UIControlState.Normal);
					//					}
					linkLabel.TextAlignment = UITextAlignment.Left;
					//					linkLabel.TextColor = new UIColor (1, 0.333f, 0.333f, 1);
					linkLabel.TextColor = new UIColor (0, 0.333f, 0.333f, 1);
					UITapGestureRecognizer labelTap = new UITapGestureRecognizer(() => {
						linkLabel.InvokeOnMainThread(delegate{
							Link link = _links[index];
							((AppDelegate)UIApplication.SharedApplication.Delegate).OpenWeb(link, _article.Level);
						} );
					} );

					linkLabel.UserInteractionEnabled = true;
					linkLabel.AddGestureRecognizer(labelTap);		

					_innerFrame.AddSubview (linkLabel);
					_linkElements.Add (linkLabel);
					linkLabel.TranslatesAutoresizingMaskIntoConstraints = false;

					if (lastUiLink == null){
						_innerFrame.AddConstraint (NSLayoutConstraint.Create (_body,NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,linkLabel,NSLayoutAttribute.Top,1, titleOffset));
					}else{
						if (lastUiLink != linkLabel){
							_innerFrame.AddConstraint (NSLayoutConstraint.Create (lastUiLink,NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,linkLabel,NSLayoutAttribute.Top,1,-8));
						}
					}

					_innerFrame.AddConstraint (NSLayoutConstraint.Create (linkLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Left, 1, 16));
					_innerFrame.AddConstraint (NSLayoutConstraint.Create (linkLabel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Right, 1, -16));
					lastUiLink = linkLabel;
				}
			}
		}		

		public void AddShareButtonToView(UIView lastUiLink){

			const int titleOffset = -32;

			var linkLabel = new UILabel (new CoreGraphics.CGRect(10,10,300,60));  // will be overwritten by contraints
			linkLabel.LineBreakMode = UILineBreakMode.WordWrap;
			linkLabel.Lines = 0;

			var attr = new UIStringAttributes {						
				Font = UIFont.SystemFontOfSize(15),
				//UnderlineStyle = NSUnderlineStyle.Single,
			} ;

			NSMutableAttributedString linkTitle;
			linkTitle = new NSMutableAttributedString (@"
JETZT TEILEN
");
			linkTitle.SetAttributes(attr.Dictionary,new NSRange(0,linkTitle.Length));
			linkLabel.LineBreakMode =  UILineBreakMode.WordWrap;
			linkLabel.TextAlignment = UITextAlignment.Center;
			linkLabel.AttributedText = linkTitle;
			linkLabel.TextColor = new UIColor (0, 0.7f, 1.0f, 1);
			UITapGestureRecognizer labelTap = new UITapGestureRecognizer(() => {
				linkLabel.InvokeOnMainThread(delegate{
                    ((AppDelegate)UIApplication.SharedApplication.Delegate).OpenShareDialog(_article);
				} );
			} );

            linkLabel.TintColor = UIColor.FromRGB(73, 217, 41);
            linkLabel.BackgroundColor = UIColor.FromRGB(73, 217, 41);
            linkLabel.TextColor = UIColor.White;

            linkLabel.UserInteractionEnabled = true;
			linkLabel.AddGestureRecognizer(labelTap);	

			_innerFrame.AddSubview (linkLabel);
			_linkElements.Add (linkLabel);
			linkLabel.TranslatesAutoresizingMaskIntoConstraints = false;


			if (lastUiLink == null){
				_innerFrame.AddConstraint (NSLayoutConstraint.Create (_body,NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,linkLabel,NSLayoutAttribute.Top,1, titleOffset));
			}else{
				if (lastUiLink != linkLabel){
					_innerFrame.AddConstraint (NSLayoutConstraint.Create (lastUiLink,NSLayoutAttribute.Bottom, NSLayoutRelation.Equal,linkLabel,NSLayoutAttribute.Top,1,-32));
				}
			}

			_innerFrame.AddConstraint (NSLayoutConstraint.Create (linkLabel, NSLayoutAttribute.Left, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Left, 1, 16));
			_innerFrame.AddConstraint (NSLayoutConstraint.Create (linkLabel, NSLayoutAttribute.Right, NSLayoutRelation.Equal, _innerFrame, NSLayoutAttribute.Right, 1, -16));
		}
	}
}

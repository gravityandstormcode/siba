﻿
using System;

using Foundation;
using UIKit;

namespace SiBa.iOS
{
	public partial class MainTableCellView : UITableViewCell
	{
		public static readonly UINib Nib = UINib.FromName ("MainTableCellView", NSBundle.MainBundle);
		public static readonly NSString Key = new NSString ("MainTableCellView");

		public MainTableCellView (IntPtr handle) : base (handle)
		{
		}

		public static MainTableCellView Create ()
		{
			return (MainTableCellView)Nib.Instantiate (null, null) [0];
		}
	}
}


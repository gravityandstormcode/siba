﻿using System;
using UIKit;
using Foundation;
using System.Collections.Generic;

namespace SiBa.iOS
{
    public class MainTableSource : UITableViewSource {
        protected SiBaArticlesFilter _filter;

        public SiBaArticlesFilter Filter{
            get
            {
                return this._filter;
            }
        }

        public MainTableSource ()
		{
            this._filter = new SiBaArticlesFilter();
		}

		public override nint NumberOfSections(UITableView tableView)
		{
			return 1;
		}

        public override nint RowsInSection(UITableView tableview, nint section)
        {
            this._filter.Filter();
            if (this._filter.FilteredArticles == null) { return 0; }
            if (this._filter.FilteredArticles.Count > 0)
            {
                return this._filter.FilteredArticles.Count;
            }
            else{
//                ((AppDelegate)UIApplication.SharedApplication.Delegate).ShowActivityIndicator(true);
                return 0;
            }
        }


        public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
            SiBaArticle article = this._filter.FilteredArticles[indexPath.Row];
			// request a recycled cell to save memory
			MainTableCell cell = tableView.DequeueReusableCell ("MainTableCell") as MainTableCell;
			cell.Article = article;
			refreshConstraints (cell);
			return cell;
		}

		public void refreshConstraints(UIView theView) {
			NSLayoutConstraint[] constraints;
			constraints = theView.Constraints;

			theView.RemoveConstraints(constraints);
			theView.SetNeedsLayout();
			theView.SetNeedsDisplay();
			theView.LayoutSubviews();
			theView.AddConstraints(constraints);
			theView.SetNeedsLayout();
			theView.SetNeedsDisplay();
			theView.LayoutSubviews();

		}
	}
}


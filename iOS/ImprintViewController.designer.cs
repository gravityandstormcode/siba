// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace SiBa.iOS
{
    [Register ("ImprintViewController")]
    partial class ImprintViewController
    {
        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Impressum { get; set; }

        [Outlet]
        [GeneratedCode ("iOS Designer", "1.0")]
        UIKit.UILabel Label { get; set; }

        void ReleaseDesignerOutlets ()
        {
            if (Impressum != null) {
                Impressum.Dispose ();
                Impressum = null;
            }

            if (Label != null) {
                Label.Dispose ();
                Label = null;
            }
        }
    }
}
﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Text;
using Android.Graphics;
using Android.Text.Style;
using Android.Graphics.Drawables;
using Plugin.Share;


namespace SiBa.Droid
{
	[Activity (Label = "DetailActivity")]			
	public class DetailActivity : AppCompatActivity
	{
		SiBaArticle _article;
		public TextView DetailTitle { get; set; }
		public TextView Body { get; set; }
		public ImageView Danger { get; set; }
        public int articleId;

		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);

            this.articleId = Intent.GetIntExtra("articleId",-1);
			_article = SiBaArticles.GetInstance ().GetArticle(articleId);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.DetailView);

			var toolbar = FindViewById<Toolbar> (Resource.Id.toolbar);

			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar (toolbar);
			SupportActionBar.Title = _article.Title;
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetIcon(Resource.Drawable.Icon);

			Color bgColor = new Color(0);
			switch(_article.Level){
			case 0:
				bgColor = new Color(0,139,108);
				break;
			case 1:
				bgColor = new Color(220,187,71);
				break;
			case 2:
				bgColor = new Color (243,88,88);
				break;
			}

			SupportActionBar.SetBackgroundDrawable(new ColorDrawable(bgColor));

			Danger = FindViewById<ImageView> (Resource.Id.Danger);
			DetailTitle = FindViewById<TextView> (Resource.Id.Title);
			Body = FindViewById<TextView> (Resource.Id.Body);

			SpannableString spannable = new SpannableString (_article.Title +"\n"+ _article.Age);
			float scaledDensity = Android.App.Application.Context.Resources.DisplayMetrics.ScaledDensity;
			spannable.SetSpan(new TextAppearanceSpan(null,TypefaceStyle.Bold,(int)Math.Round(18*scaledDensity),null,null),0,_article.Title.Length,SpanTypes.ExclusiveExclusive);
			spannable.SetSpan(new TextAppearanceSpan(null,TypefaceStyle.Normal,(int)Math.Round(12*scaledDensity),null,null),_article.Title.Length,_article.Title.Length + _article.Age.Length+1,SpanTypes.ExclusiveExclusive);
			DetailTitle.SetText (spannable, TextView.BufferType.Spannable);
			Body.Text = _article.Body;

			switch(_article.Level){
			case 0:
				Danger.SetImageResource (Resource.Drawable.danger0);
				break;
			case 1:
				Danger.SetImageResource (Resource.Drawable.danger1);
				break;
			case 2:
				Danger.SetImageResource (Resource.Drawable.danger2);
				break;
			}

			LinearLayout root = FindViewById<LinearLayout> (Resource.Id.DetailView);

            string headerAction = "Wie schütze ich mich?";
            SiBa.Link linkHeaderAction = new SiBa.Link();
            linkHeaderAction.Style = Link.LinkStyle.TitleLinkNoLink;
            linkHeaderAction.Title = headerAction;
            root.AddView(MainViewCellHolder.AddTextView(linkHeaderAction));

            foreach (Link link in _article.url_action)
            {
                root.AddView(MainViewCellHolder.AddButton(link, false));
            }

            string headerExplanation = "Wer kann mir helfen?";
            SiBa.Link linkHeaderExplanation = new SiBa.Link();
            linkHeaderExplanation.Style = Link.LinkStyle.TitleLinkNoLink;
            linkHeaderExplanation.Title = headerExplanation;
            root.AddView(MainViewCellHolder.AddTextView(linkHeaderExplanation));

            foreach (Link link in _article.url_explanation)
            {
                root.AddView(MainViewCellHolder.AddButton(link, false));
            }

            root.AddView(MainViewCellHolder.AddShareLink(_article));
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override bool OnCreateOptionsMenu(IMenu menu)
        {
            MenuInflater.Inflate(Resource.Menu.detail, menu);
            return base.OnCreateOptionsMenu(menu);
        }

        public override bool OnOptionsItemSelected (IMenuItem item)
		{
			if (item.ItemId == Android.Resource.Id.Home)
				Finish ();
            if (item.ItemId == Resource.Id.menu_share  ||  item.ItemId == 2131296389 || item.ItemId == 2131296390 || item.ItemId == 2131296391) {
				Plugin.Share.Abstractions.ShareMessage msg = new Plugin.Share.Abstractions.ShareMessage();
				msg.Title = DetailTitle.Text;
				msg.Text = DetailTitle.Text;
				msg.Url = "https://www.sicher-im-netz.de/node/" + articleId;
				CrossShare.Current.Share(msg);

//                CrossShare.Current.ShareLink("https://www.sicher-im-netz.de/node/" + articleId, DetailTitle.Text, DetailTitle.Text);

            }
            return base.OnOptionsItemSelected (item);
		}
	}
}


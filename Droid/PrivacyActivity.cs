﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.Graphics.Drawables;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Support.V7.App;
using Android.Text;

namespace SiBa.Droid
{
	[Activity(Label = "PrivacyActivity")]
	public class PrivacyActivity : AppCompatActivity
	{
		protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Privacy);

			TextView text = (TextView)FindViewById(Resource.Id.PrivacyBody);

			ISpanned res;
			String html = GetString(Resource.String.privacy_body);
			html = "<br/><br/><big>Impressum</big>" +
				"<br/>Deutschland sicher im Netz e.V." +
				"<br/>Albrechtstraße 10 b" +
				"<br/>10117 Berlin" +
				"<br/><br/>Tel. +49 (0) 30 27576-310" +
				"<br/>Fax +49 (0) 30 27576-51310" +
				"<br/>info@sicher-im-netz.de" +
				"<br/><br/>Geschäftsführer: Dr. Michael Littger" +
				"<br/><br/>Vereinsregister des AG Berlin-Charlottenburg: VR-Nr. 27217 B" +
				"<br/>Verantwortlich für die redaktionellen Inhalte:" +
				"<br/>Dr. Michael Littger<br/><br/>" +
				"<br/>Verfasser aller SiBa - Meldungen ist Deutschland sicher im Netz e.V. Mehr Informationen zur SiBa-App finden Sie unter <a href='https://www.sicher-im-netz.de/siba'> www.sicher-im-netz.de/siba</a> <br/><br/>" +
    			"<big>Nutzungsbedingungen</big>" +
				"<br/><br/><b>Bereitstellung der Informationen/Gewährleistung/Haftung</b>"  +
				"<br/>Alle in dieser App und ihren Teilen veröffentlichten Informationen und Angaben wurden im Rahmen des Zumutbaren sorgfältig recherchiert, zusammengestellt und überprüft. Sie dienen ausschließlich zur unverbindlichen, persönlichen Information des Nutzers der App. Für Richtigkeit, Vollständigkeit und Aktualität der Angaben kann weder eine Garantie noch Haftung übernommen werden. Wir behalten uns das Recht vor, die in dieser App und ihren Teilen bereitgestellten Informationen ohne vorherige Ankündigung zu ändern oder zu ergänzen. Dies gilt auch für die Nutzungsbedingungen, deren jeweils aktuellste Version maßgeblich ist. Die Haftung für Inhalte und Services dieser Website ist auf Vorsatz und grobe Fahrlässigkeit begrenzt." +
				"<br/><br/><b>Zulässige Nutzung der Website (Schutzrechte/Copyright)</b>" +
				"<br/>Die App und ihre Teile sind urheberrechtlich geschützt. Dies betrifft auch die Struktur, die Navigation und die Gestaltung der App. Darüber hinaus sind geschützte Rechte an Namen oder Marken sowie sonstige Schutzrechte Dritter zu beachten." +
				"<br/><br/>Wir gewähren Ihnen das Recht, die im Rahmen dieser App bereitgestellten Text ganz oder auch ausschnittsweise zu speichern und zu vervielfältigen. Aus Gründen des Urheberrechts ist allerdings eine Speicherung und Vervielfältigung von Bildmaterial, Grafiken, Ton- und Videomaterial, die über die Verwendung in Cache- (temporären Verzeichnis) oder Proxy-Systemen oder die bloße Anzeige auf dem Bildschirm hinausgeht nicht gestattet. Eine darüber hinausgehende Nutzung der bereitgestellten Inhalte ist über die Verwendung unseres RSS-Feeds sowie im Rahmen einer Einbindung per iframe möglich - kontaktieren Sie uns hierzu per E-Mail (siba@sicher-im-netz.de)." +
				"<br/><br/>Jede Verwendung oder Vervielfältigung der bereitgestellten Inhalte und Daten für andere Zwecke als der der persönlichen Information ist nur nach vorheriger schriftlicher Genehmigung zulässig. Ausnahmen: Sind Sie Journalist, so stehen Ihnen alle Informationen in unserem Pressebereich für Ihre redaktionelle Tätigkeit zur freien Verwendung, wir erbitten jedoch ein Belegexemplar bzw. Link auf die mit unseren Informationen erstellte Publikation/Artikel." +
				"<br/><br/>Mit dem Besuch dieser Website verpflichten Sie sich, jede missbräuchliche Nutzung der bereitgestellten Inhalte und Services zu unterlassen, insbesondere keine Sicherheitsvorkehrungen zu umgehen sowie keine Aktionen auszuführen, die zu Schäden an Servern oder Netzwerken führen. Darüber hinaus verpflichten Sie sich, die bereitgestellten Inhalte und Services nicht in andere Internetangebote zu integrieren oder kommerziell zu nutzen." +
				"<br/><br/><b>Links/Services von Kooperationspartnern</b>" +
				"<br/>Innerhalb dieser App und ihren Teilen verweisen Verbindungen (\"Links\") direkt oder indirekt auf weitere Websites oder andere Internet-Angebote. Wir möchten ausdrücklich betonen, dass wir keinerlei Einfluss auf die Gestaltung und die Inhalte der so erreichbaren Websites und Internet-Angebote haben. Die Verantwortung für diese Websites und Internet-Angebote tragen die jeweiligen Anbieter; wir sind für diese Websites und Internet-Angebote nicht verantwortlich." +
				"<br/><br/><b>Geltendes Recht</b>" +
				"<br/>Die Nutzung dieser Website unterliegt deutschem Recht mit Ausnahme seiner kollisionsrechtlichen Bestimmungen" +
				"<br/><br/><b>Nutzung von Daten</b>" +
				"<br/>Die App erfasst keine personenbezogenen Daten." +
				"<br/><br/><b>Änderungen an diesen Datenschutzbestimmungen</b>" +
				"<br/>Diese Datenschutzbestimmungen werden in unregelmäßigen Abständen aktualisiert. Wir empfehlen Ihnen, diese Datenschutzbestimmungen regelmäßig zu lesen, um über den Schutz der von uns erfassten persönlichen Daten auf dem Laufenden zu bleiben. Durch die fortgesetzte Nutzung des Dienstes erklären Sie sich mit diesen Datenschutzbestimmungen und deren Aktualisierungen einverstanden."
				;
			if (Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.N)
			{
				res = Html.FromHtml(html, Android.Text.FromHtmlOptions.ModeLegacy);
			}else{
#pragma warning disable CS0618
				res = Html.FromHtml(html);
#pragma warning restore CS0618
			}
			text.TextFormatted = res;
			text.MovementMethod = Android.Text.Method.LinkMovementMethod.Instance;

			// Set our view from the "main" layout resource 
			var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);

			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar(toolbar);

			SupportActionBar.Title = "Impressum & Datenschutz";
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetIcon(Resource.Drawable.Icon);
			SupportActionBar.SetBackgroundDrawable(new ColorDrawable(new Color(100, 180, 70)));
		}

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override bool OnOptionsItemSelected(IMenuItem item)
		{
			if (item.ItemId == Android.Resource.Id.Home)
			{
				Finish();
			}
			return base.OnOptionsItemSelected(item);
		}
	}
}

﻿/*
using System;
using Android.Content;
using Android.App;
using Android.Gms.Gcm;
using Android.OS;
using Android.Support.V7.App;

namespace SiBa.Droid
{
	[Service (Exported = false), IntentFilter (new [] { "com.google.android.c2dm.intent.RECEIVE" })]
	public class PushNotificationListenerService : GcmListenerService
	{
		public PushNotificationListenerService ()
		{
		}

		public override void OnMessageReceived (string from, Bundle data)
		{
			var message = data.GetString ("message");
			System.Diagnostics.Debug.WriteLine ("MyGcmListenerService", "From:    " + from);
			System.Diagnostics.Debug.WriteLine ("MyGcmListenerService", "Message: " + message);
			SendNotification (message);
		}

		void SendNotification (string message)
		{
			var intent = new Intent (this, typeof(MainActivity));
			intent.AddFlags (ActivityFlags.ClearTop);
			var pendingIntent = PendingIntent.GetActivity (this, 0, intent, PendingIntentFlags.OneShot);

			var notificationBuilder = new NotificationCompat.Builder(this)
				.SetSmallIcon (Resource.Drawable.Icon)
				.SetContentTitle ("SiBa")
				.SetContentText (message)
				.SetAutoCancel (true)
				.SetContentIntent (pendingIntent);

			var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
			notificationManager.Notify (0, notificationBuilder.Build());
		}

	}
}
*/


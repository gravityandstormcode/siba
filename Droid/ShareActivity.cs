﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Graphics.Drawables;
using Android.Graphics;
using Android.Support.Design.Widget;
using System.Text.RegularExpressions;
using System.Net.Http;

namespace SiBa.Droid
{
    [Activity (Icon = "@drawable/ic_launcher", RoundIcon ="@drawable/ic_launcher", Label = "Nachricht an DsiN senden", ParentActivity = typeof(MainActivity), Exported = true)]
    [IntentFilter(new[] { Intent.ActionSend },
        Categories = new[] { Intent.CategoryDefault, "SiBa" },
        Icon = "@drawable/ic_launcher",
        DataMimeType =  "text/plain")]
    public class ShareActivity : AppCompatActivity
    {
        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            Xamarin.Essentials.Platform.Init(this, savedInstanceState);

            SetContentView(Resource.Layout.Share);

            // Set our view from the "main" layout resource 
            var toolbar = FindViewById<Toolbar>(Resource.Id.toolbar);

            //Toolbar will now take on default actionbar characteristics
            SetSupportActionBar(toolbar);

            SupportActionBar.Title = "Nachricht an DsiN senden";
            SupportActionBar.SetDisplayHomeAsUpEnabled(true);
            SupportActionBar.SetIcon(Resource.Drawable.Icon);
            SupportActionBar.SetBackgroundDrawable(new ColorDrawable(new Color(100, 180, 70)));

            if (Intent.ActionSend.Equals(Intent.Action) && Intent.Type != null)
            {
                if ("text/plain".Equals(Intent.Type))
                {
                    TextInputEditText editText = FindViewById<TextInputEditText>(Resource.Id.share_link);
                    editText.Text = Intent.GetStringExtra(Intent.ExtraText);
                }
            }


            Button button1 = FindViewById<Button>(Resource.Id.send_button);
            button1.Click += (sender, e) => {

                TextInputEditText fromField = FindViewById<TextInputEditText>(Resource.Id.share_email);
                TextInputEditText subjectField = FindViewById<TextInputEditText>(Resource.Id.share_subject);
                TextInputEditText linkField = FindViewById<TextInputEditText>(Resource.Id.share_link);
                EditText linkMessage = FindViewById<EditText>(Resource.Id.share_message);

                string from = fromField.Text;
                if (from == "") { from = "benutzerhatkeineadresseangegeben@siba.com"; }

                if (Regex.Match(from, @"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$").Success)
                {

                }
                else
                {

                    Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
                    alert.SetTitle("Fehler");
                    alert.SetMessage("Die eigegebene Emailadresse scheint ungültig zu sein.");
                    alert.SetPositiveButton("Weiter", (senderAlert, args) => {
                    });

                    Dialog dialog = alert.Create();
                    dialog.Show();
                    return;
                }


                bool isAllEmpty = true;


                string title = subjectField.Text;
                if (title == "") { title = "Benutzer hat keinen Titel angegeben."; } else { isAllEmpty = false; }

                string link = linkField.Text;
                if (link == "") { link = "Benutzer hat keinen Link angegeben."; } else { isAllEmpty = false; }

                string body = linkMessage.Text;
                if (body == "") { body = "Benutzer hat kein Nachricht angegeben."; } else { isAllEmpty = false; }

                string code = SiBa.Keys.SendGridCode;
                string url = SiBa.Keys.SendGridURL;
                string to = "siba@dsin.de";

                if (!isAllEmpty)
                {
                    string RestUrl = url + "code=" + code + "&from=" + from + "&to=" + to + "&link=" + link + "&title=" + title + "&message=" + body;
                    SendEmailAndWait(RestUrl);
                }
            };
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        async void SendEmailAndWait(string url)
        {
            HttpClient client = new HttpClient();
            var response = await client.GetAsync(url);
            ShowAlert();
        }

        void ShowAlert()
        {

            Android.Support.V7.App.AlertDialog.Builder alert = new Android.Support.V7.App.AlertDialog.Builder(this);
            alert.SetTitle("Vielen Dank für Ihre Nachricht.");
            alert.SetMessage("Die Daten wurden an Deutschland sicher im Netz weitergeleitet.");
            alert.SetPositiveButton("OK", (senderAlert, args) => {
                Finish();
            });

            Dialog dialog = alert.Create();
            dialog.Show();
        }

    }
}

﻿using Android.App;
using Android.Content;
using Android.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using WindowsAzure.Messaging;


using Firebase.Iid;

namespace SiBa.Droid
{
    [Service]
    [IntentFilter(new[] { "com.google.firebase.INSTANCE_ID_EVENT" })]
    public class FirebaseRegistrationService : FirebaseInstanceIdService
    {
        NotificationHub hub = null;
        const string TAG = "FirebaseRegistrationService";

        public override void OnTokenRefresh()
        {
            var refreshedToken = FirebaseInstanceId.Instance.Token;
            Log.Debug(TAG, "Refreshed token: " + refreshedToken);

            // save the token
            var prefs = Application.Context.GetSharedPreferences("SiBa", FileCreationMode.Private);
            var prefEditor = prefs.Edit();
            prefEditor.PutString("pushtoken", refreshedToken);
            prefEditor.Commit();

            PushManager PushManager = new PushManager();
            PushManager.UpdatePushSettings();
            // SendRegistrationTokenToAzureNotificationHub(refreshedToken);
        }

        
    }
}
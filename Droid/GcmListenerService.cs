﻿using Android.App;
using Android.Content;
using Android.OS;
using Android.Gms.Gcm;
using Android.Util;

namespace SiBa.Droid
{
    [Service(Exported = false), IntentFilter(new[] { "com.google.android.c2dm.intent.RECEIVE" })]
    public class MyGcmListenerService : GcmListenerService
    {
        public override void OnMessageReceived(string from, Bundle data)
        {
            var message = data.GetString("message");
            Log.Debug("MyGcmListenerService", "From:    " + from);
            Log.Debug("MyGcmListenerService", "Message: " + message);
            SendNotification(message);
        }

        void SendNotification(string message)
        {
            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);

            var flags = PendingIntentFlags.OneShot;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.S)
            {
                flags |= PendingIntentFlags.Immutable;
            }
            PendingIntent pendingIntent = PendingIntent.GetActivity(this, 0, intent, flags);

            // Ensure that you're setting the channel ID here
            var notificationBuilder = new Notification.Builder(this, "my_notification_channel")
                .SetSmallIcon(Resource.Drawable.ic_push)
                .SetContentTitle("GCM Message")
                .SetContentText(message)
                .SetAutoCancel(true)
                .SetContentIntent(pendingIntent);

            var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(0, notificationBuilder.Build());
        }

    }
}
﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Android.Support.V4.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Webkit;
using Android.Graphics.Drawables;
using Android.Graphics;

namespace SiBa.Droid
{
	[Activity (Label = "WebActivity")]			
	public class WebActivity : AppCompatActivity
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);

            string title = Intent.GetStringExtra("title");
			string url = Intent.GetStringExtra("url");

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.WebView);

			// Set our view from the "main" layout resource 
			var toolbar = FindViewById<Toolbar> (Resource.Id.toolbar);

			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar (toolbar);

			SupportActionBar.Title = title;
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetBackgroundDrawable(new ColorDrawable(new Color(100,180,70)));

			WebView webView = FindViewById<WebView> (Resource.Id.webView);
			webView.LoadUrl (url);

		}

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        public override bool OnOptionsItemSelected (IMenuItem item)
		{
			if (item.ItemId == Android.Resource.Id.Home)
				Finish ();

			return base.OnOptionsItemSelected (item);
		}

	}
}


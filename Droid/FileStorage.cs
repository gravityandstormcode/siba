﻿using System;
using System.IO;
using Newtonsoft.Json;
using Android.Content;
using System.Collections.Generic;

namespace SiBa.Droid
{
	public class FileStorage : IFileStorage
	{
		public FileStorage ()
		{
			Context context = Android.App.Application.Context;
			try{
				var prefs =  context.GetSharedPreferences(context.PackageName, FileCreationMode.Private);
				string file = prefs.GetString ("settings.json", string.Empty);
				_cache = JsonConvert.DeserializeObject<Data>(file); // comment for debugging to get empty settings
				if (_cache == null){
					_cache = new Data();
				}
                // check if any of the needed objects was not filled with data from the cache. If so, init them now.
                if (_cache.Categories == null) {
                    _cache.Categories = new List<SiBaCategory>();
                }
                if (_cache.Articles == null) {
                    _cache.Articles = new List<SiBaArticle>();
                }
                if (_cache.CategoryState == null) {
                    _cache.CategoryState = new Dictionary<int, bool>();
                }
                if (_cache.DangerState == null) {
                    _cache.DangerState = new Dictionary<int, bool>();
                    // initialize danger states
                    _cache.DangerState[0] = false;
                    _cache.DangerState[1] = false;
                    _cache.DangerState[2] = true;
                }
            }
			catch(Exception e){
				System.Diagnostics.Debug.WriteLine (e.ToString ());
			}
		}


        public Boolean cacheOK() {
            // check if any of the needed objects was not filled with data from the cache. If so, init them now.
            if (_cache.Categories == null)
            {
                return false; 
            }
            if (_cache.Articles == null)
            {
                return false;
            }
            if (_cache.CategoryState == null)
            {
                return false;
            }
            if (_cache.DangerState == null)
            {
                return false;
            }
            else {
                if (_cache.DangerState.Count < 1) {
                    return false;
                }
            }
            return true;
        }

		override public void Flush (){
			if (_isDirty)
			{
				string output = JsonConvert.SerializeObject(_cache);

				Context context = Android.App.Application.Context;
				var prefs =  context.GetSharedPreferences(context.PackageName, FileCreationMode.Private);
				var prefEditor = prefs.Edit();
				prefEditor.PutString ("settings.json", output);
				prefEditor.Commit();

				_isDirty = false;
			}
		}
	}
}


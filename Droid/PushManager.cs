﻿using Android.App;
using Android.Content;
using Android.Util;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using WindowsAzure.Messaging;
using Firebase.Iid;

namespace SiBa.Droid
{
    public class PushManager
    {


        public PushManager() {

        }

        public void UpdatePushSettings()
        {
            NotificationHub hub = new NotificationHub(SiBa.Keys.NotificationHubName,
                      SiBa.Keys.NotificationHubEndpoint, Android.App.Application.Context);

            //retrieve token from settings 
            var prefs = Application.Context.GetSharedPreferences("SiBa", FileCreationMode.Private);
            string token = prefs.GetString("pushtoken", null);
            Log.Debug("UpdatePushSettings", $"Token retrieved: {token}");
            Boolean DebugKategorie = prefs.GetBoolean("pushdebug", false);

            var tags = new List<string>() { };
            SiBaSettings settings = SiBaSettings.GetInstance();

            Boolean Red = settings.GetDangerState(2);
            Boolean Yellow = settings.GetDangerState(1);
            Boolean Green = settings.GetDangerState(0);

            List<SiBaCategory> Categories = settings.GetCategories();
            foreach (SiBaCategory category in Categories)
            {
                try
                {
                    category.State = settings.GetCategoryState(category.Id);
                    if (category.State == true)
                    {
                        if (Red == true)
                        {
                            tags.Add("red_" + category.Id.ToString());
                        }
                        if (Yellow == true)
                        {
                            tags.Add("yellow_" + category.Id.ToString());
                        }
                        if (Green == true)
                        {
                            tags.Add("green_" + category.Id.ToString());
                        }
                    }
                }
                catch (Exception)
                {

                }
            }
            if (DebugKategorie == true)
            {
                tags.Add("debug_debug");
            }

            Log.Debug("UpdatePushSettings", $"Tags to register: {string.Join(", ", tags)}");

            try
            {
                Task.Run(() => {
                    var regID = hub.Register(token, tags.ToArray()).RegistrationId;
                    Log.Debug("FirebaseRegistrationService", $"Successful registration of ID {regID}");
                });
            }
            catch (Exception ex)
            {
                Log.Error("FirebaseRegistrationService", $"Error during registration: {ex.Message}");
            }

        }

    }
}
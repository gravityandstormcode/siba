﻿using System;
using Android.Support.V7.Widget;
using SiBa;
using Android.Views;
using System.Collections.Generic;

namespace SiBa.Droid
{
	public class MainViewAdapter : RecyclerView.Adapter
	{

        SiBaArticlesFilter _filter = new SiBaArticlesFilter();
		public MainViewAdapter ()
		{
		}

		public override int ItemCount{
			get{
                this._filter.Filter();
                List<SiBaArticle> filteredArticles = this._filter.FilteredArticles;
                if (filteredArticles != null)
                {
                    return filteredArticles.Count;
                }
                else
                {
                    return 0;
                }
			}
		}

        public void Filter() {
            this._filter.Filter();
        }

        public override RecyclerView.ViewHolder OnCreateViewHolder(ViewGroup parent, int viewType)
		{
			var layoutInflater = LayoutInflater.From(parent.Context);
			if(viewType == 1){
				var view = layoutInflater.Inflate(Resource.Layout.MainViewCell, parent, false);
				var viewHolder = new MainViewCellHolder(view);
				return viewHolder;

			}
			return null;
		}

		public override void OnBindViewHolder(RecyclerView.ViewHolder holder, int position)
		{
			((MainViewCellHolder)holder).Bind(this._filter.FilteredArticles[position]);
		}

		public override int GetItemViewType(int position)
		{
			List<SiBaArticle> articles = this._filter.FilteredArticles;
			if (articles.Count > position){
				return 1;
			}
			return 0;
		}

	}
}


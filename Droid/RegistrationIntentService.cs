﻿using System;

using Android.Gms.Gcm;
using Android.Gms.Gcm.Iid;
using Android.Content;
using Android.Util;
using Android.App;


namespace SiBa.Droid
{
	[Service(Exported = false)]
	public class RegistrationIntentService : IntentService
	{
		static object locker = new object();

		public RegistrationIntentService() : base("RegistrationIntentService") { }

		protected override void OnHandleIntent (Intent intent)
		{
			try
			{
				Log.Info ("RegistrationIntentService", "Calling InstanceID.GetToken");
				lock (locker)
				{
					var instanceID = InstanceID.GetInstance (this);
					var token = instanceID.GetToken (
						"812910700274", GoogleCloudMessaging.InstanceIdScope, null);

					Log.Info ("RegistrationIntentService", "GCM Registration Token: " + token);
					SendRegistrationToAppServer (token);
					Subscribe (token);
				}
			}
			catch (Exception e)
			{
				Log.Debug("RegistrationIntentService", "Failed to get a registration token");
				Log.Debug("RegistrationIntentService", e.ToString ());
				return;
			}
		}

		async void SendRegistrationToAppServer (string token)
		{
			// Add custom implementation here as needed.
			SiBaNotification notification = new SiBaNotification();
			await notification.registerToken (token);
		}

		void Subscribe (string token)
		{
			var pubSub = GcmPubSub.GetInstance(this);
			pubSub.Subscribe(token, "/topics/global", null);
		}
	}
}


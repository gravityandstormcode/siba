﻿using System;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.OS;
using Android.Support.V7.Widget;
using System.Collections.Generic;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Graphics.Drawables;
using Android.Graphics;
using Firebase.Iid;
using Firebase.Messaging;

using Android.Support.V4.App;
using Firebase;
using Android.Support.V4.Content;
using Android.Content.PM;

namespace SiBa.Droid
{
	[Activity (Label = "SiBa", MainLauncher = true, Theme = "@style/Theme.App.Starting", Icon = "@drawable/ic_launcher", RoundIcon = "@drawable/ic_launcher")]
	public class MainActivity : AppCompatActivity
	{
		private FileStorage _storage = new FileStorage();
		private MainViewAdapter _mainViewAdapter = new MainViewAdapter();
		private RecyclerView _recyclerView;
        const int PERMISSIONS_REQUEST_POST_NOTIFICATIONS = 100; 

        protected override void OnCreate (Bundle bundle)
		{
            // Handle the splash screen transition.
            AndroidX.Core.SplashScreen.SplashScreen.InstallSplashScreen(this);

            base.OnCreate(bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);
            FirebaseApp.InitializeApp(Application.Context);
            string token = FirebaseInstanceId.Instance.Token;

            // save the token
            if (token != null)
            {
                var prefs = Application.Context.GetSharedPreferences("SiBa", FileCreationMode.Private);
                var prefEditor = prefs.Edit();
                prefEditor.PutString("pushtoken", token);
                prefEditor.Commit();
            }

            SiBaSettings.GetInstance().Init(_storage, "android");
            if (_storage.cacheOK() == false) {
                SiBaSettings.GetInstance().SetArticles(null);
                SiBaSettings.GetInstance().SetCategories(null);
            }

			SiBaArticles.GetInstance ().Load ();
			SiBaCategories.GetInstance ().Load ();

			SiBaArticles.GetInstance ().AddOnFinishedCallback (OnNewsLoadedEvent);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			var toolbar = FindViewById<Toolbar> (Resource.Id.toolbar);

			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar (toolbar);
			SupportActionBar.SetIcon(Resource.Drawable.Icon);
			SupportActionBar.SetBackgroundDrawable(new ColorDrawable(new Color(100,180,70)));

			SupportActionBar.Title = "SiBa";
			
			SetupUiElements();

            //Task.Run(() =>
            //{
            //FirebaseInstanceId.Instance.DeleteInstanceId();
            //Console.WriteLine("Forced Token:" + FirebaseInstanceId.Instance.Token);
            //});

            CreateNotificationChannel();

            PushManager PushManager = new PushManager();
            PushManager.UpdatePushSettings();

            if ((int)Build.VERSION.SdkInt >= 33)
            {
                if (ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.PostNotifications) != Permission.Granted)
                {
                    ActivityCompat.RequestPermissions(this, new string[] { Android.Manifest.Permission.PostNotifications }, PERMISSIONS_REQUEST_POST_NOTIFICATIONS);
                }
            }

        }


        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            switch (requestCode)
            {
                case PERMISSIONS_REQUEST_POST_NOTIFICATIONS:
                    {
                        if (grantResults.Length > 0 && grantResults[0] == Permission.Granted)
                        {
                            // Permission was granted, so we do nothing.
                        }
                        else
                        {
                            // Permission was denied
                            // Check if we should show rationale
                            if (ActivityCompat.ShouldShowRequestPermissionRationale(this, permissions[0]))
                            {
                                // Here, user denied without 'Don't ask again'
                                // This just means the dialog will appear again at the next app start.
                            }
                            else
                            {
                                // User checked 'Don't ask again', do not bother them anymore
                            }
                        }
                        return;
                    }
            }
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        //public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        //{
        //    Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        //
        //            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        //        }

        override protected void OnResume()
        {
            _mainViewAdapter.Filter();
            _mainViewAdapter.NotifyDataSetChanged();
            base.OnResume();
        }

        private void OnNewsLoadedEvent(List<SiBaArticle> newArticles){
			_mainViewAdapter.NotifyDataSetChanged ();
		}


		private void SetupUiElements()
		{
			_mainViewAdapter = new MainViewAdapter();
			_recyclerView = FindViewById<RecyclerView>(Resource.Id.recyclerGridView);
			var sglm = new StaggeredGridLayoutManager(1, StaggeredGridLayoutManager.Vertical);

			_recyclerView.SetLayoutManager(sglm);
			_recyclerView.SetAdapter(_mainViewAdapter);
		}

		public override bool OnCreateOptionsMenu (IMenu menu)
		{
			MenuInflater.Inflate (Resource.Menu.home, menu);
			return base.OnCreateOptionsMenu (menu);
		}
		public override bool OnOptionsItemSelected (IMenuItem item)
		{	
			switch(item.ItemId) {
    			case Resource.Id.menu_settings:
    				StartActivity (typeof(SettingsActivity));
                    break;
                case Resource.Id.menu_partner:
    				StartActivity (typeof(PartnerActivity));
                    break;
                case Resource.Id.menu_imprint:
    				StartActivity(typeof(PrivacyActivity));
                    break;
                case Resource.Id.menu_share:
                    StartActivity(typeof(ShareActivity));
                    break;
                case Resource.Id.menu_filter:
                    Intent activity = new Intent(this, typeof(SettingsActivity));
                    activity.PutExtra("isFilter", true);
                    StartActivity(activity);
                    break;
		    }
            return base.OnOptionsItemSelected(item);
        }

        void CreateNotificationChannel()
        {
            if (Build.VERSION.SdkInt < BuildVersionCodes.O)
            {
                // Notification channels are new in API 26 (and not a part of the
                // support library). There is no need to create a notification
                // channel on older versions of Android.
                return;
            }

            var channel = new NotificationChannel("my_notification_channel",
                                                  "FCM Notifications",
                                                  NotificationImportance.High)
            {

                Description = "Firebase Cloud Messages appear in this channel"
            };

            var notificationManager = (NotificationManager)GetSystemService(Android.Content.Context.NotificationService);
            notificationManager.CreateNotificationChannel(channel);
        }
    }






    [Service]
    [IntentFilter(new[] { "com.google.firebase.MESSAGING_EVENT" })]
    public class FirebaseNotificationService : FirebaseMessagingService
    {
        public override void OnMessageReceived(RemoteMessage message)
        {
            Console.WriteLine("MESSAGE RECEIVED!!!!");
            if (message.Data.ContainsKey("message"))
            {
                SendNotification(message.Data["message"]); // message.GetNotification().Body;
            }
            else
            {
#if DEBUG
                SendNotification("Debug message von SiBa Android");
#endif

            }
        }

        void SendNotification(string messageBody)
        {
            Console.WriteLine("SEND NOTIFICATION!!!!");

            var intent = new Intent(this, typeof(MainActivity));
            intent.AddFlags(ActivityFlags.ClearTop);

            // Setup an intent for SecondActivity:
            Intent secondIntent = new Intent(this, typeof(MainActivity));
            // Create a task stack builder to manage the back stack:
            Android.App.TaskStackBuilder stackBuilder = Android.App.TaskStackBuilder.Create(this);
            // Add all parents of SecondActivity to the stack:
            stackBuilder.AddParentStack(Java.Lang.Class.FromType(typeof(MainActivity)));
            // Push the intent that starts SecondActivity onto the stack:
            stackBuilder.AddNextIntent(secondIntent);


            const int pendingIntentId = 0;
            var flags = PendingIntentFlags.OneShot;
            if (Build.VERSION.SdkInt >= BuildVersionCodes.S)
            {
                flags |= PendingIntentFlags.Immutable;
            }
            PendingIntent pendingIntent = PendingIntent.GetActivity(this, 0, intent, flags);


            //PendingIntent pendingIntent = PendingIntent.GetActivity(this, pendingIntentId, intent, PendingIntentFlags.OneShot | PendingIntentFlags.Immutable);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(this, "my_notification_channel")
              .SetContentIntent(pendingIntent)
              .SetSmallIcon(Resource.Drawable.ic_launcher)
              .SetContentTitle("Sicherheitsbarometer")
              .SetContentText(messageBody);

            builder.SetDefaults((int)(NotificationDefaults.Sound | NotificationDefaults.Vibrate));

            var notificationManager = (NotificationManager)GetSystemService(Context.NotificationService);
            notificationManager.Notify(1, builder.Build());
        }


    }
}



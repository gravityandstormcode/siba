﻿using System;
using Android.Support.V7.Widget;
using Android.Widget;
using Android.Views;
using Android.Text;
using Android.Text.Style;
using Android.Graphics;
using SiBa;
using System.Collections.Generic;
using Android.Content;
using Android.OS;
using Android.Util;
using Plugin.Share;

namespace SiBa.Droid
{

	public class MainViewCellHolder : RecyclerView.ViewHolder
	{
		public LinearLayout Cell { get; set;}
		public TextView Title { get; set; }
		public TextView Body { get; set; }
		public ImageView Danger { get; set; }
        public LinearLayout LinksAction { get; set; }
        public LinearLayout LinksExplanation { get; set; }


        private SiBaArticle _article=null;
        private List<TextView> _buttonsAction = null;
        private List<TextView> _buttonsExplanations = null;

        public MainViewCellHolder(View itemView) : base(itemView)
		{
			Cell = itemView.FindViewById<LinearLayout> (Resource.Id.Cell);
			Danger = itemView.FindViewById<ImageView> (Resource.Id.Danger);
			Title = itemView.FindViewById<TextView> (Resource.Id.Title);
            Body = itemView.FindViewById<TextView>(Resource.Id.Body);
            LinksAction = itemView.FindViewById<LinearLayout>(Resource.Id.actions);
            LinksExplanation = itemView.FindViewById<LinearLayout>(Resource.Id.explanations);

            ((LinearLayout)itemView.FindViewById (Resource.Id.Cell)).Click += delegate {
				Intent intent = new Intent(Android.App.Application.Context, typeof(DetailActivity));
				Bundle b = new Bundle();
				b.PutInt("articleId",_article.Id);
				intent.PutExtras(b); 
				intent.SetFlags(ActivityFlags.NewTask);
				Android.App.Application.Context.StartActivity(intent);
			};
		}

		static public Button AddButton(Link link, bool isHeadline){
			float scaledDensity = Android.App.Application.Context.Resources.DisplayMetrics.ScaledDensity;
            int padding = 0; // (int)Math.Round(10 * scaledDensity);

            Button linkButton = new Button(Android.App.Application.Context);

            if (isHeadline)
            {
                linkButton.SetTextSize((Android.Util.ComplexUnitType)1, 18);
            }
            else {
                linkButton.SetTextSize((Android.Util.ComplexUnitType)1, 14);
            }

            linkButton.SetText(link.Title, TextView.BufferType.Normal);
            linkButton.PaintFlags = PaintFlags.UnderlineText;
            linkButton.SetBackgroundResource(0);
			linkButton.Gravity = GravityFlags.Left | GravityFlags.CenterVertical;
			linkButton.SetTextColor (Color.Rgb(30,30,30));
			linkButton.SetPadding (padding, (int)Math.Round(5*scaledDensity), 0, 0);
			linkButton.Tag = link.Url;
			linkButton.TransformationMethod =null;

            linkButton.LayoutParameters = new ViewGroup.LayoutParams(
				ViewGroup.LayoutParams.MatchParent,
				ViewGroup.LayoutParams.WrapContent
				);
			linkButton.Click += delegate(object sender, EventArgs e) {

				Intent intent = new Intent(Android.App.Application.Context, typeof(WebActivity));
				Bundle b = new Bundle();
				b.PutString("title", link.Title);
				b.PutString("url", link.Url);
				intent.PutExtras(b); 
				intent.SetFlags(ActivityFlags.NewTask);
				Android.App.Application.Context.StartActivity(intent);

			};
				
			return linkButton;
		}

        static public Button AddShareLink(SiBaArticle article)
        {
            float scaledDensity = Android.App.Application.Context.Resources.DisplayMetrics.ScaledDensity;
            int padding = 0; // (int)Math.Round(10 * scaledDensity);

            Button linkButton = new Button(Android.App.Application.Context);
            linkButton.SetTextSize((Android.Util.ComplexUnitType)1, 18);

            linkButton.SetText("JETZT TEILEN", TextView.BufferType.Normal);
//            linkButton.PaintFlags = PaintFlags.UnderlineText;
            linkButton.SetBackgroundResource(0);
            linkButton.Gravity = GravityFlags.Right | GravityFlags.CenterVertical;
            linkButton.SetTextColor(Color.Rgb(0, 162, 255));
            linkButton.SetPadding(padding, (int)Math.Round(5 * scaledDensity), 0, 0);
            linkButton.TransformationMethod = null;

            linkButton.LayoutParameters = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.WrapContent
                );
            linkButton.Click += delegate (object sender, EventArgs e) {

				// Share a link and an optional title and message.
				Plugin.Share.Abstractions.ShareMessage msg = new Plugin.Share.Abstractions.ShareMessage();
				msg.Title = article.Body;
				msg.Text = article.Body;
				msg.Url = "https://www.sicher-im-netz.de/node/" + article.Id;
				CrossShare.Current.Share(msg);
				// Share a link and an optional title and message.
//				CrossShare.Current.ShareLink("https://www.sicher-im-netz.de/node/" + article.Id, article.Body, article.Title);
            };

            return linkButton;
        }



        static public TextView AddTextView(Link link)
        {
            float scaledDensity = Android.App.Application.Context.Resources.DisplayMetrics.ScaledDensity;

            TextView linkTextView = new TextView(Android.App.Application.Context);
            linkTextView.SetText(link.Title, TextView.BufferType.Normal);
            linkTextView.SetBackgroundResource(0);
            linkTextView.Gravity = GravityFlags.Left | GravityFlags.CenterVertical;
            linkTextView.SetTextColor(Color.Rgb(30, 30, 30));
            linkTextView.SetPadding(0, (int)Math.Round(5 * scaledDensity), 0, 0);
            linkTextView.Tag = link.Url;
            linkTextView.TransformationMethod = null;
            linkTextView.SetTextSize((Android.Util.ComplexUnitType) 1, 18);
            linkTextView.LayoutParameters = new ViewGroup.LayoutParams(
                ViewGroup.LayoutParams.MatchParent,
                ViewGroup.LayoutParams.WrapContent
                );
            return linkTextView;
        }


        private void AddButtonInternal_action(Link link){
			Button linkButton = AddButton (link, false);
            LinksAction.AddView(linkButton);
            _buttonsAction.Add (linkButton);
		}

        private void AddButtonInternal_explanations(Link link, bool isHeadline)
        {
            Button linkButton = AddButton(link, isHeadline);
            LinksExplanation.AddView(linkButton);
            _buttonsExplanations.Add(linkButton);
        }

        private void AddTextViewActionsInternal(Link link)
        {
            TextView linkTextView = AddTextView(link);
            LinksAction.AddView(linkTextView);
            _buttonsAction.Add(linkTextView);
        }

        private void AddTextViewExplanationsInternal(Link link)
        {
            TextView linkTextView = AddTextView(link);
            LinksExplanation.AddView(linkTextView);
            _buttonsExplanations.Add(linkTextView);
        }

        public void Bind(SiBaArticle article){
			SpannableString spannable = new SpannableString (article.Title +"\n"+ article.Age);

			float scaledDensity = Android.App.Application.Context.Resources.DisplayMetrics.ScaledDensity;
			spannable.SetSpan(new TextAppearanceSpan(null,TypefaceStyle.Bold,(int)Math.Round(18*scaledDensity),null,null),0,article.Title.Length,SpanTypes.ExclusiveExclusive);
			spannable.SetSpan(new TextAppearanceSpan(null,TypefaceStyle.Normal,(int)Math.Round(12*scaledDensity),null,null),article.Title.Length,article.Title.Length + article.Age.Length+1,SpanTypes.ExclusiveExclusive);
			Title.SetText (spannable, TextView.BufferType.Spannable);
			Body.Text = article.Summary;
			switch(article.Level){
			case 0:
				Danger.SetImageResource (Resource.Drawable.danger0);
				break;
			case 1:
				Danger.SetImageResource (Resource.Drawable.danger1);
				break;
			case 2:
				Danger.SetImageResource (Resource.Drawable.danger2);
				break;
			}


			if (_buttonsExplanations != null && _article != article){
				foreach(TextView text in _buttonsExplanations)
                {
					LinksExplanation.RemoveView (text);
				}
                _buttonsExplanations = new List<TextView> ();
			}else if (_buttonsExplanations == null){
                _buttonsExplanations = new List<TextView> ();
			}

            if (_buttonsAction != null && _article != article)
            {
                foreach (TextView text in _buttonsAction)
                {
                    LinksAction.RemoveView(text);
                }
                _buttonsAction = new List<TextView>();
            }
            else if (_buttonsAction == null)
            {
                _buttonsAction = new List<TextView>();
            }

            _article = article;
            int arrayIndex = 0;

            if (_buttonsAction.Count == 0){

                string headerAction = "Wie schütze ich mich?";
                SiBa.Link linkHeaderAction = new SiBa.Link();
                linkHeaderAction.Style = Link.LinkStyle.TitleLinkNoLink;
                linkHeaderAction.Title = headerAction;
                AddTextViewActionsInternal(linkHeaderAction);

                foreach (Link link in article.url_action)
                {
                    // if (link.Style == Link.LinkStyle.TitleLinkNoLink)
                    // {
                    //    AddTextViewActionsInternal(link);
                    //}
                    //else
                    //{
                        AddButtonInternal_action(link);
                    //}
                    arrayIndex++;
                }
            }

            if (_buttonsExplanations.Count == 0)
            {
                arrayIndex = 0;

                string headerExplanation = "Wer kann mir helfen?";
                SiBa.Link linkHeaderExplanation = new SiBa.Link();
                linkHeaderExplanation.Style = Link.LinkStyle.TitleLinkNoLink;
                linkHeaderExplanation.Title = headerExplanation;
                AddTextViewExplanationsInternal(linkHeaderExplanation);

                foreach (Link link in article.url_explanation)
                {
                    //if (_article.url_explanation.Count == 1)
                    //{
                    //    AddButtonInternal_explanations(link, true);
                    //}
                    //else
                    //{
                    //    if (link.Style == Link.LinkStyle.TitleLinkNoLink)
                    //    {
                    //        AddTextViewExplanationsInternal(link);
                    //    }
                    //    else
                    //    {
                            AddButtonInternal_explanations(link, false);
                    //    }
                    //}
                }
            }                
        }
	}
}


﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.Support.V7.App;
using Toolbar = Android.Support.V7.Widget.Toolbar;
using Android.Graphics.Drawables;
using Android.Graphics;
using static Android.Content.Res.Resources;
using Android.Support.V4.Content;
using Android.Content.PM;
using Android.Support.V4.App;

namespace SiBa.Droid
{
    [Activity(Label = "SettingsActivity")]
    public class SettingsActivity : AppCompatActivity
    {
        bool isFilter = false;

        protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate(bundle);
            Xamarin.Essentials.Platform.Init(this, bundle);

            this.isFilter = Intent.GetBooleanExtra("isFilter", false);


            float scaledDensity = Android.App.Application.Context.Resources.DisplayMetrics.ScaledDensity;

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.SettingsPage);
			LinearLayout root = FindViewById<LinearLayout> (Resource.Id.settings_root);


			Switch danger_0 = FindViewById<Switch> (Resource.Id.toggle_danger_0);
			Switch danger_1 = FindViewById<Switch> (Resource.Id.toggle_danger_1);
			Switch danger_2 = FindViewById<Switch> (Resource.Id.toggle_danger_2);

            SiBaSettings settings = SiBaSettings.GetInstance();

            danger_0.Checked = isFilter ? settings.GetFilterDangerState(0) : settings.GetDangerState (0);
			danger_1.Checked = isFilter ? settings.GetFilterDangerState(1) : settings.GetDangerState(1);
            danger_2.Checked = isFilter ? settings.GetFilterDangerState(2) : settings.GetDangerState(2);

            danger_0.Tag = 0;
			danger_1.Tag = 1;
			danger_2.Tag = 2;

			danger_0.Click += OnDangerCheckedChanged;
			danger_1.Click += OnDangerCheckedChanged;
			danger_2.Click += OnDangerCheckedChanged;

			danger_0.SetPadding(0,(int)Math.Round(scaledDensity * 5),0,(int)Math.Round(scaledDensity * 5));
			danger_1.SetPadding(0,(int)Math.Round(scaledDensity * 5),0,(int)Math.Round(scaledDensity * 5));
			danger_2.SetPadding(0,(int)Math.Round(scaledDensity * 5),0,(int)Math.Round(scaledDensity * 5));


			List<SiBaCategory> categories = SiBaCategories.GetInstance ().Categories;
			if (categories != null){
				foreach(SiBaCategory category in categories){
					Switch categorySwitch = new Switch (this);
					categorySwitch.Text = category.Title;
					categorySwitch.Tag = category.Id;
					categorySwitch.Checked = isFilter ? settings.GetFilterCategoryState(category.Id) : settings.GetCategoryState(category.Id);
					categorySwitch.SetPadding(0,(int)Math.Round(scaledDensity * 5),0,(int)Math.Round(scaledDensity * 5));
					categorySwitch.Click += OnCategoryCheckedChanged;
					root.AddView (categorySwitch);
				}
			}

			// Set our view from the "main" layout resource 
			var toolbar = FindViewById<Toolbar> (Resource.Id.toolbar);

			//Toolbar will now take on default actionbar characteristics
			SetSupportActionBar (toolbar);

			SupportActionBar.Title = this.isFilter ? "Filter" : "Push-Notifikationen";
			SupportActionBar.SetDisplayHomeAsUpEnabled(true);
			SupportActionBar.SetIcon(Resource.Drawable.Icon);
			SupportActionBar.SetBackgroundDrawable(new ColorDrawable(new Color(100,180,70)));


            TextView headlineFilter = FindViewById<TextView>(Resource.Id.settings_filter_title);
            headlineFilter.Text = this.isFilter ? "Filter" : "Push-Notifikationen";

            TextView subline = FindViewById<TextView>(Resource.Id.settings_subline);

            if (isFilter)
            {
                subline.Text = "Hier können Sie die angezeigten SiBa-Meldungen filtern. Hier gemachte Einstellungen haben keinen Einfluss darauf, welche Push-Benachrichtigungen Sie erhalten.";
            }
            else
            {
                subline.Text = "Hier können Sie einstellen, für welche SiBa-Meldungen Sie eine Push-Benachrichtigung erhalten. Hier gemachte Einstellungen haben keinen Einfluss darauf, welche SiBa-Meldungen Ihnen in der App angezeigt werden.";
            }


        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, Android.Content.PM.Permission[] grantResults)
        {
            Xamarin.Essentials.Platform.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }

        protected override void OnPause(){
			SiBaSettings.GetInstance ().Flush ();
			base.OnPause ();
		}

		public override bool OnOptionsItemSelected (IMenuItem item)
		{
			if (item.ItemId == Android.Resource.Id.Home)
				Finish ();

			return base.OnOptionsItemSelected (item);
		}

		public void OnDangerCheckedChanged(Object sender, System.EventArgs args){
            const int PERMISSIONS_REQUEST_POST_NOTIFICATIONS = 100;
            Switch danger = (Switch)sender;
            SiBaSettings settings = SiBaSettings.GetInstance();
            if (isFilter)
            {
                settings.SetFilterDangerState((int)danger.Tag, danger.Checked);
                settings.Flush();
            }
            else
            {
                if (danger.Checked)
                {
                    // Check if we have the required permission
                    if ((int)Build.VERSION.SdkInt >= 33 && ContextCompat.CheckSelfPermission(this, Android.Manifest.Permission.PostNotifications) != Permission.Granted)
                    {


                        // Request the permission
                        ActivityCompat.RequestPermissions(this, new string[] { Android.Manifest.Permission.PostNotifications }, PERMISSIONS_REQUEST_POST_NOTIFICATIONS);
                        Toast.MakeText(this, "Bitte Benachrichtigungen aktivieren für Pushnachrichten.", ToastLength.Long).Show();

                        Intent intent = new Intent();
                        intent.SetAction(Android.Provider.Settings.ActionApplicationDetailsSettings);
                        Android.Net.Uri uri = Android.Net.Uri.FromParts("package", "de.dsin.siba", null);
                        intent.SetData(uri);
                        StartActivity(intent);

                    }
                }
                settings.SetDangerState((int)danger.Tag, danger.Checked);
                PushManager PushManager = new PushManager();
                PushManager.UpdatePushSettings();
            }
        }


        public void OnCategoryCheckedChanged(Object sender, System.EventArgs args) {
			Switch danger = (Switch)sender;
            SiBaSettings settings = SiBaSettings.GetInstance();
            if (isFilter)
            {
                settings.SetFilterCategoryStage((int)danger.Tag, danger.Checked);
                settings.Flush();
            }
            else
            {
                settings.SetCategoryState((int)danger.Tag, danger.Checked);
                PushManager PushManager = new PushManager();
                PushManager.UpdatePushSettings();
            }
        }
    }
}


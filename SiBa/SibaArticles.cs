﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Net;
using System.IO;
using System.Diagnostics;

namespace SiBa
{
	public class SiBaArticles : DataFetcher<SiBaArticle>
	{
		private static SiBaArticles _instance=null;
		private List<SiBaArticle> _articles=null;

		public SiBaArticles ()
		{
//			clearStoredData (); // for debugging only!
		}

		public static SiBaArticles GetInstance(){
			if (_instance == null){
				_instance = new SiBaArticles ();
			}
			return _instance;
		}

		public async void Load(){
			SiBaSettings settings = SiBaSettings.GetInstance ();

			// first, load stored articles
			try{
				_articles = settings.GetArticles();
			}
			catch(Exception){
			}

			if (_articles != null && _onLoadedHandler != null){
				_onLoadedHandler (_articles);
			}

			// then load articles (server now only delivers last 15 Articles)
			_articles = await fetchData (settings.Domain + "siba/list/0", true);

            SiBaSettings.GetInstance().SetArticles(_articles);
            settings.Flush();
            if (_onLoadedHandler != null){
				_onLoadedHandler (_articles);
			}
		}

		protected void clearStoredData(){
			SiBaSettings.GetInstance().SetLastUpdate(0);
			SiBaSettings.GetInstance().SetArticles(null);
        }

        public List<SiBaArticle> Articles{
			get{
				return _articles;
			}
		}			
		public SiBaArticle GetArticle(int id){
			foreach(SiBaArticle article in _articles){
				if (article.Id == id){
					return article;
				}
			}
			return null;
		}
	}
}


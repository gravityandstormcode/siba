﻿using System;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace SiBa
{
	public class Tracking
	{
		private static Tracking _instance;
		private const string _id = "4";
		private const string _baseUrl ="https://www.dsin-blog.de/piwik/piwik.php";
		private string _customerId;

		private Tracking ()
		{
			_customerId = ((DateTime.Now - new DateTime (1970, 1, 1)).TotalMilliseconds / 1000).ToString();
		}

		public static Tracking GetInstance(){
			if (_instance == null){
				_instance = new Tracking();
			}
			return _instance;
		}

		public void Track(string action, string key=null, string value=null){
			try{
				string actionUrl = action;
				if (key != null){
					actionUrl += "/" + key;
				}
				if(value != null){
					actionUrl += "/" + value;
				}

				Dictionary<string, string> data = new Dictionary<string, string> ();
				data.Add ("idsite", _id);
				data.Add ("action_name", actionUrl);
				data.Add ("_id", _customerId);
				data.Add ("c_var", "{\"1\":[\"OS\",\"+SiBaSettings.GetInstance().Os+\"]}");
				data.Add ("rec", "1");
				data.Add ("url", "/"+actionUrl);

				string dataUrl="";

				foreach(KeyValuePair<string, string> entry in data)
				{
					dataUrl += (dataUrl.Length == 0 ? "?" : "&");
					dataUrl += entry.Key + "=" + entry.Value;
				}
				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create (new Uri (_baseUrl+ dataUrl));
				request.Method = "GET";

				request.BeginGetResponse (null, null);				
			}
			catch(Exception){
				
			}
		}
	}
}


﻿using System;
using System.Collections.Generic;

namespace SiBa
{
	public class SiBaCategories : DataFetcher<SiBaCategory>
	{

		private static SiBaCategories _instance=null;
		private List<SiBaCategory> _categories=null;

		public SiBaCategories ()
		{
		}

		public static SiBaCategories GetInstance(){
			if (_instance == null){
				_instance = new SiBaCategories ();
			}
			return _instance;
		}

		public List<SiBaCategory> Categories{
			get{
				return _categories;
			}
		}

		public async void Load(){
			SiBaSettings settings = SiBaSettings.GetInstance ();

			try{
				_categories = settings.GetCategories();
                if (_categories != null)
                {
                    foreach (SiBaCategory category in _categories)
                    {
                        try
                        {
                            category.State = settings.GetCategoryState(category.Id);
                        }
                        catch (Exception)
                        {

                        }
                    }
                }
            }
			catch(Exception){
				
			}

            // Fetch all categories from the server
			_categories = await fetchData(settings.Domain + "siba/categories");
            // Now transfer the values the user has already choosen with the list we got from the server
            if (_categories != null)
            {
                foreach (SiBaCategory category in _categories)
                {
                    try
                    {
                        category.State = settings.GetCategoryState(category.Id);
                    }
                    catch (Exception){}
                }
                // With the Categorielist and the user choosen values combined set this list as the new data
                settings.SetCategories(_categories);
            }

            settings.Flush();
            if (_onLoadedHandler != null)
            {
                _onLoadedHandler(_categories);
            }
        }
    }
}



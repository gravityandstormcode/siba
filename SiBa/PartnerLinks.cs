﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SiBa
{
    public class PartnerLinks
    {
        public static Link Dsin()
        {
            return new Link("https://www.sicher-im-netz.de/", "Deutschland sicher im Netz e.V.");
        }
        public static Link Bka()
        {
            return new Link("http://www.bka.de/", "Bundeskriminalamt");
        }
        public static Link Telekom()
        {
            return new Link("http://www.telekom.de/", "Telekom Deutschlang GmbH");
        }
        public static Link Gdv()
        {
            return new Link("http://www.gdv.de/", "GDV Gesamtverband der Deutschen Versicherungswirtschaft e.V.");
        }
        public static Link Bundesdruckerei()
        {
            return new Link("https://www.bundesdruckerei.de/de", "Bundesdruckerei GmbH");
        }
        public static Link Bsi()
        {
            return new Link("https://www.bsi.bund.de/", "Bundesamt für Sicherheit in der Informationstechnik");
        }
        public static Link Nokia()
        {
            return new Link("http://www.nokia.com/", "Nokia");
        }
        public static Link Microsoft()
        {
            return new Link("https://www.microsoft.com/de-de", "Microsoft");
        }
        public static Link Bankenverband()
        {
            return new Link("http://www.bankenverband.de/", "Bundesverband deutscher Banken e.V.");
        }
        public static Link FireEye()
        {
            return new Link("https://www.sparkasse.de/", "Sparkasse: Produkte und Services - Sparkasse.de");
        }

        public static Link Eco()
        {
            return new Link("https://www.eco.de/", "eco - Verband der Internetwirtschaft e.V.");
        }

        public static Link Polizei()
        {
            return new Link("http://www.polizei-beratung.de/", "Polizei - Wir wollen, dass Sie sicher leben.");
        }
        public static Link Avira()
        {
            return new Link("http://www.avira.com/", "Sicherheitssoftware für Windows, Mac, Android und iOS.");
        }
        public static Link Bmi()
        {
            return new Link("http://www.bmi.bund.de", "Bundesministerium des Innern, für Bau und Heimat - Bundesinnenministerium");
        }
    }
}


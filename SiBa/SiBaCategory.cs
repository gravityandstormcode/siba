﻿using System;

namespace SiBa
{
	public class SiBaCategory
	{

		private bool _state;
		public string Title { get; set;}
		public int Id { get; set;}
		public bool State{
			get{
				return _state;
			}
			set{
				_state = value;
			}
		}

		public SiBaCategory ()
		{
		}


	}
}


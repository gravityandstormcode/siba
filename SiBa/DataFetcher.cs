﻿using System;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Text;
using System.Net.Http;

namespace SiBa
{
	public class DataFetcher<T>
	{
		protected OnDataLoaded _onLoadedHandler;

		public delegate void OnDataLoaded(List<T> newData);

		public DataFetcher ()
		{
		}
			

		protected async Task<List<T>> fetchData (string url, bool postToken = false)
		{
			try{
				// Create an HTTP web request using the URL:

				HttpClient client = new HttpClient();
				var postData = new List<KeyValuePair<string, string>>();
				if (postToken){
					postData.Add(new KeyValuePair<string, string>("token", SiBaSettings.GetInstance().GetToken()));
					postData.Add(new KeyValuePair<string, string>("settings", SiBaSettings.GetInstance().GetSettingsJson()));
				}

				HttpContent content = new FormUrlEncodedContent(postData);
				HttpResponseMessage msg = await client.PostAsync(new Uri (url),content).ConfigureAwait(false);

				JsonReader jsonReader = new JsonTextReader (new StreamReader (await msg.Content.ReadAsStreamAsync().ConfigureAwait(false)));
				JsonSerializer serializer = new JsonSerializer ();

				List<T> dataList = serializer.Deserialize<List<T>>(jsonReader);
				return dataList;

			}
			catch(Exception e){
				System.Diagnostics.Debug.WriteLine (e.ToString ());
				return new List<T>();
			}
		}

		public void AddOnFinishedCallback(OnDataLoaded onLoadedHandler){
			_onLoadedHandler += onLoadedHandler;
		}

	}
}


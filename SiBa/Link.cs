﻿using System;

namespace SiBa
{
	public class Link
	{
        private string _title;
        public enum LinkStyle
        {
            Normal=0,
            TitleLink=1,
            TitleLinkNoLink=2
        }
		public string Title
        {
            get
            {
                return _title;
            }
            set{
                _title = value.Trim();
            }
        }
		public string Url{get; set;}
        public LinkStyle Style { get; set; }
		public Link ()
		{
            Style = LinkStyle.Normal;
		}

        public Link(string url, string title)
        {
            Style = LinkStyle.Normal;
            Url = url;
            Title = title;
        }
	}
}


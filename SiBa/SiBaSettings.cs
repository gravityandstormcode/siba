﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;
using System.IO;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;
using System.Net;
using System.Text;
using System.Net.Http;

#if __IOS__
using Akavache;
#endif

namespace SiBa
{
	public class SiBaSettings
	{

		private static SiBaSettings _instance;
		private IStorage _storage;
		private string _os;
		private bool _settingsDirty;
        private bool _initDone;
        private int _summaryCutoff = 160;


		private SiBaSettings(){
			_settingsDirty = false;
            _initDone = false;
        }

		~SiBaSettings(){
		}

		public void Init(IStorage storage, string os){
			_storage = storage;
			_os = os;
			Tracking.GetInstance().Track ("AppStarted");
		}

        public IStorage storage {
            get {
                return _storage;
            }
        }

        public bool InitDone {
            get {
                return _initDone;
            }
            set {
                _initDone = value;
            }
        }

		public string Os{
			get{
				return _os;
			}
		}

        public int SummaryCutoff
        {
            get
            {
                return _summaryCutoff;
            }
            set
            {
                _summaryCutoff = value;
            }
        }

		public string Domain{
			get{
#if DEBUG
//                return "https://www.sicher-im-netz.de/siba-app/";
//                return "http://192.168.178.52/siba-app/"; // TODO: Remove!
				return "https://www.sicher-im-netz.de/siba-app/";
//                return "http://192.168.1.106/sicher-im-netz-de/siba-app/";
#else
                                return "https://www.sicher-im-netz.de/siba-app/";
                //                return "http://www.danielyan.de/siba/";
//                return "http://192.168.178.52/"; // TODO: Remove!
#endif
            }
        }

		public static SiBaSettings GetInstance(){
			if (_instance == null){
				_instance = new SiBaSettings ();
            }
			return _instance;
		}
			
		public bool GetDangerState(int level){
			if (_storage != null){
				return _storage.GetDangerState(level);
			}
			return false;
		}

		public void SetDangerState(int level, bool state){
			if (_storage != null){
				_settingsDirty = true;
				_storage.SetDangerState(level, state);
			}
		}

		public string GetSettingsJson(){
			JObject json = new JObject ();
			// gather all three danger states
			json["danger"] = new JObject();
			for(int i=0;i < 3; ++i){
				try{
					json ["danger"][i.ToString()] = GetDangerState (i);
				}
				catch(Exception){
				}
			}
			// gather all category states
			json["category"] = new JObject();
			SiBaCategories categories = SiBaCategories.GetInstance();
			if (categories != null && categories.Categories != null){
				foreach(SiBaCategory category in categories.Categories){
					json ["category"][category.Id.ToString()] = category.State;
				}
			}
			return json.ToString ().Replace('\n',' ');
		}

		public async Task<bool> SendSettings(){
			if (_settingsDirty){
				try{

					HttpClient client = new HttpClient();
					var postData = new List<KeyValuePair<string, string>>();

					postData.Add(new KeyValuePair<string, string>("token", GetToken()));
					postData.Add(new KeyValuePair<string, string>("settings", GetSettingsJson()));

					HttpContent content = new FormUrlEncodedContent(postData);

					await client.PostAsync(new Uri (Domain + "siba/settings/set"),content).ConfigureAwait(false);
					_settingsDirty = false;
					return true;
				}
				catch(Exception e){
					System.Diagnostics.Debug.WriteLine (e.ToString ());
				}
			}
			return false;
		}

		public bool GetCategoryState(int id){
			return _storage.GetCategoryState (id);
		}

		public void SetCategoryState(int id, bool state){
			_storage.SetCategoryState (id, state);
		}

		public List<SiBaCategory> GetCategories(){
			return _storage.GetCategories ();
		}

		public void SetCategories(List<SiBaCategory> categories){
			_storage.SetCategories (categories);
		}

        public bool GetFilterCategoryState(int id)
        {
            return _storage.GetFilterCategoryState(id);
        }

        public void SetFilterCategoryStage(int id, bool state)
        {
            _storage.SetFilterCategoryState(id, state);
        }

        public bool GetFilterDangerState(int level)
        {
            return _storage.GetFilterDangerState(level);
        }

        public void SetFilterDangerState(int level, bool state)
        {
            _storage.SetFilterDangerState(level, state);
        }

        public async void Flush(){
			 _storage.Flush();
			await SendSettings ();
		}

		public string GetToken(){
/*			
#if DEBUG				
			return "6da15227da305ca051da10f61e5dd2184d4df4b437022ab258b7d0af5bd81f5";
#else
*/
			return _storage.GetToken ();
//#endif
		}

		public void SetToken(string token){
			_storage.SetToken (token);
		}

        public Boolean GetDebugTag()
        {
            return _storage.GetDebugTag();
        }

        public void SetDebugTag(Boolean value)
        {
            _storage.SetDebugTag(value);
        }




        public List<SiBaArticle> GetArticles(){
			return _storage.GetArticles ();
		}

		public void SetArticles(List<SiBaArticle> articles){
			_storage.SetArticles (articles);
		}

		public long GetLastUpdate(){
			return _storage.GetLastUpdate ();
		}
		public void SetLastUpdate(long lastUpdate){
			_storage.SetLastUpdate (lastUpdate);
		}
	}
}


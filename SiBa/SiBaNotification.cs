﻿using System;
using System.Threading.Tasks;
using System.Text;
using System.Net.Http;
using System.Collections.Generic;

namespace SiBa
{
	public class SiBaNotification
	{

		public SiBaNotification ()
		{
		}

		~SiBaNotification(){
		}


		public async Task<bool> registerToken (string token)
		{
			// clean up token
			if (!string.IsNullOrWhiteSpace(token)) {
				token = token.Trim ('<').Trim ('>').Replace (" ","");
			}

			// Get previous device token
			string oldDeviceToken = null;
			try{
				oldDeviceToken = SiBaSettings.GetInstance ().GetToken();
			}
			catch(Exception){
			}

			// Has the token changed?
			if (string.IsNullOrEmpty (oldDeviceToken) || !oldDeviceToken.Equals (token)) {

				string url = SiBaSettings.GetInstance().Domain + "siba/push_notifications";

				HttpClient client = new HttpClient ();
				client.DefaultRequestHeaders
					.Accept
					.Add (new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue ("application/json"));

				string json = "{\"token\":\"" + token + "\"," +
					"\"type\":\"" + SiBaSettings.GetInstance().Os + "\"}";

                /*
				HttpContent content = new StringContent(json,Encoding.UTF8,"application/json");
				try{
					await client.PostAsync(new Uri (),content).ConfigureAwait(false);
					SiBaSettings.GetInstance ().SetToken(token);
                    return true;
				}
				catch(Exception e){
					System.Diagnostics.Debug.WriteLine (e.ToString ());
				}
				*/
			}
            return false;
        }

    }
}



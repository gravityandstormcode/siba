﻿using System;
using System.Collections.Generic;

namespace SiBa
{
	public abstract class IFileStorage : IStorage
	{
		protected class Data{
            public Data() {
				Categories = new List<SiBaCategory>();
				Articles = new List<SiBaArticle>();
			
				CategoryState = new Dictionary<int, bool>();

                DangerState = new Dictionary<int, bool>();

				// initialize danger states
				DangerState[0] = false;
				DangerState[1] = false;
				DangerState[2] = true;

                FilterCategoryState = new Dictionary<int, bool>();
                FilterDangerState = new Dictionary<int, bool>();
                // initialize filter danger states
                FilterDangerState[0] = true;
                FilterDangerState[1] = true;
                FilterDangerState[2] = true;
            }
            public string Token{ get; set;}
			public List<SiBaCategory> Categories{get;set;}
			public List<SiBaArticle> Articles{ get; set;}
			public long LastUpdate{ get; set;}
			public Dictionary<int, bool> DangerState{ get; set;}
			public Dictionary<int, bool> CategoryState{ get; set;}
            public Dictionary<int, bool> FilterDangerState { get; set; }
            public Dictionary<int, bool> FilterCategoryState { get; set; }
            public Boolean DebugTag { get; set; }
        }

        protected Data _cache = null;
        protected bool _isDirty=false;

		public IFileStorage ()
		{
            _cache = new Data();
		}

		override public void SetArticles(List<SiBaArticle> articles){
			_cache.Articles = articles;
            _isDirty = true;
        }

		override public List<SiBaArticle> GetArticles(){
            if (_cache.Articles == null)
            {
                _cache.Articles = new List<SiBaArticle>();
            }
			return _cache.Articles;
		}

		override public void SetCategories(List<SiBaCategory> categories){
			_cache.Categories = categories;
            _isDirty = true;
        }

        override public List<SiBaCategory> GetCategories(){
            if (_cache.Categories == null) {
                _cache.Categories = new List<SiBaCategory>();
            }
			return _cache.Categories;
		}

		override public void SetToken(string token){
			_cache.Token = token;
            _isDirty = true;
        }
        override public string GetToken(){
			return _cache.Token;
		}

        override public void SetDebugTag(Boolean value)
        {
            _cache.DebugTag = value;
            _isDirty = true;
        }
        override public Boolean GetDebugTag()
        {
            return _cache.DebugTag;
        }


        override public void SetLastUpdate(long lastUpdate){
			_cache.LastUpdate = lastUpdate;
            _isDirty = true;
        }

        override public long GetLastUpdate(){
            return _cache.LastUpdate;
		}

		override public void SetDangerState(int level, bool state){
            _cache.DangerState [level] = state;
            _isDirty = true;
        }

        override public bool GetDangerState(int level){
            if (_cache.DangerState == null) {
                _cache.DangerState = new Dictionary<int, bool>();

                // initialize danger states
                _cache.DangerState[0] = false;
                _cache.DangerState[1] = false;
                _cache.DangerState[2] = true;
            }
            return _cache.DangerState[level];
		}

		override public void SetCategoryState (int id, bool state){
			_cache.CategoryState [id] = state;
            _isDirty = true;
        }

        override public bool GetCategoryState(int id){
            if (_cache.CategoryState == null) {
                _cache.CategoryState = new Dictionary<int, bool>();
            }
            if (_cache.CategoryState.ContainsKey(id))
            {
                return _cache.CategoryState[id];
            }
			return true;
		}



        override public void SetFilterDangerState(int level, bool state){
            _cache.FilterDangerState[level] = state;
            _isDirty = true;
        }

        override public bool GetFilterDangerState(int level){
            if (_cache.FilterDangerState == null)
            {
                _cache.FilterDangerState = new Dictionary<int, bool>();

                // initialize danger states
                _cache.FilterDangerState[0] = true;
                _cache.FilterDangerState[1] = true;
                _cache.FilterDangerState[2] = true;
            }
            return _cache.FilterDangerState[level];
        }

        override public void SetFilterCategoryState(int id, bool state)
        {
            _cache.FilterCategoryState[id] = state;
            _isDirty = true;
        }

        override public bool GetFilterCategoryState(int id)
        {
            if (_cache.FilterCategoryState == null)
            {
                _cache.FilterCategoryState = new Dictionary<int, bool>();
            }
            if (_cache.FilterCategoryState.ContainsKey(id))
            {
                return _cache.FilterCategoryState[id];
            }
            return true;
        }

    }
}

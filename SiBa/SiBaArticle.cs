﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace SiBa
{
	public class SiBaArticle
	{
		private DateTime _publishedDate;
		private List<int> _categories;
		private List<Link> _actionUrls;
		private List<Link> _explanationUrls;
		private string _title;
		private string _body;
		private int _id;
		private int _level;

		public string published_date{
			set{
				double ticks = double.Parse(value)*1000;
				TimeSpan time = TimeSpan.FromMilliseconds(ticks);
				_publishedDate = new DateTime(1970, 1, 1) + time;				
			}
			get{
				return ((_publishedDate - new DateTime (1970, 1, 1)).TotalMilliseconds / 1000).ToString();
			}
		}

		private int GetYears(TimeSpan timespan)
		{
			return (int)Math.Ceiling(timespan.TotalDays/365.2425);
		}
		private int GetMonths(TimeSpan timespan)
		{
			return (int)Math.Ceiling(timespan.TotalDays/30.436875);
		}
		private int GetWeeks(TimeSpan timespan)
		{
			return (int)Math.Ceiling((timespan.TotalDays+timespan.Minutes/100.0)/7.0);
		}

		public string Age{
            set { }
			get{
				DateTime now = DateTime.Now;
                var cultureInfo = new CultureInfo("de-DE");
                return _publishedDate.ToString("dd. MMMMM", cultureInfo);

/*
				TimeSpan difference = (DateTime.Today - _publishedDate);
				if (difference.TotalMilliseconds > 0){

                    var cultureInfo = new CultureInfo("de-DE");
                    var dateTimeInfo = cultureInfo.DateTimeFormat;

                    int years = GetYears (new DateTime(now.Year, 1, 1) - _publishedDate);
					if (years > 0){
						if (years == 1){
							return "Letztes Jahr";
						}
                        return dateTimeInfo.GetMonthName(_publishedDate.Month) + " " + _publishedDate.ToString("yyyy");
					}

					int months = GetMonths (new DateTime(now.Year, now.Month, 1) - _publishedDate);
					if (months > 0){
						if (months == 1){
							return "Letzten Monat";
						}
                        return dateTimeInfo.GetMonthName(_publishedDate.Month);
					}

					int dayOfWeek = (int)DateTime.Now.DayOfWeek-1;
					if (dayOfWeek == -1){
						dayOfWeek = 6;
					}

					if (dayOfWeek < Math.Ceiling(difference.TotalDays)){
						return "Letzte Woche";
					}

					int weeks = GetWeeks (now.AddDays(-(dayOfWeek)) - _publishedDate);

					if (weeks > 1){
						return _publishedDate.ToString("dd MMMMM");
					}

					if (difference.TotalDays < 1){
						return "Gestern";
					}
					if (difference.Days < 2){
						return "Vorgestern";
					}

                    return dateTimeInfo.GetDayName(_publishedDate.DayOfWeek);
				}

				difference = (DateTime.Now - _publishedDate);

				const string prefix = "Vor ";

				if (difference.Hours > 0){
					if (difference.Hours == 1){
						return prefix + " einer Stunde";
					}
					return prefix + difference.Hours + " Stunden";
				}

				if (difference.Minutes <= 1){
					return "Gerade eben";
				}
				return prefix + difference.Minutes + " Minuten";
*/				
			}
		}

		public List<int> Categories{
			set{
				_categories = value;	
			}
			get{
				return _categories;
			}
		}

		public bool IsActive(){
			SiBaCategories categories = SiBaCategories.GetInstance ();
			foreach(int c in _categories){
				if (categories.Categories[c].State){
					return true;
				}
			}
			return false;
		}

		public int Level{
			set{
				_level = value;
			}
			get{
				return _level;
			}
		}

		public string Title{
			set{
				_title = value;
			}
			get{
				return _title;
			}
		}

		public string Body{
			set{
				_body = value;
			}

			get{
				return _body;
			}
		}

		public int Id{
			set{
				_id = value;
			}
			get{
				return _id;
			}
		}

		public List<Link> url_action{
			set{
				_actionUrls = value;
                /*
                string title = "Wie schütze ich mich?";
                if (_actionUrls.Count > 1)
                {
                    Link titleLink =  new Link();
                    titleLink.Style = Link.LinkStyle.TitleLinkNoLink;
                    titleLink.Title = title;
                    _actionUrls.Insert(0, titleLink);
                }
                else
                {
                    _actionUrls[0].Style = Link.LinkStyle.TitleLink;
                    _actionUrls[0].Title = title;
                }
                */

            }
			get{
				return _actionUrls;
			}
		}

		public List<Link> url_explanation{
			set{
				_explanationUrls = value;
				/*
                string title = "Wer kann mir helfen?";
                if (_explanationUrls.Count > 1)
                {
                    Link titleLink = new Link();
                    titleLink.Style = Link.LinkStyle.TitleLinkNoLink;
                    titleLink.Title = title;
                    _explanationUrls.Insert(0, titleLink);
                }
                else
                {

                    _explanationUrls[0].Style = Link.LinkStyle.TitleLink;
                    _explanationUrls[0].Title = title;
                }
                */
			}
			get
            {
				return _explanationUrls;
			}
		}

		public string Summary{
			get{
				int cutoff = SiBaSettings.GetInstance().SummaryCutoff;
				if (Body.Length > cutoff){
					string summary = Body.Substring (0, cutoff);
					int lastSpace = summary.LastIndexOf (' ');
					if (lastSpace > cutoff -20){
						summary = summary.Substring (0, lastSpace);
					}
					return summary + "... Weiterlesen";
				}
				return Body;
			}
		}

		public SiBaArticle ()
		{
		}
	}
}


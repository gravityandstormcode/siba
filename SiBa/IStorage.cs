﻿using System;
using System.Collections.Generic;

namespace SiBa
{
	public abstract class IStorage
	{
		public IStorage ()
		{
		}

		abstract public void Flush ();

		abstract public void SetArticles(List<SiBaArticle> articles);

		abstract public List<SiBaArticle> GetArticles();

		abstract public void SetCategories(List<SiBaCategory> categories);

		abstract public List<SiBaCategory> GetCategories();

		abstract public void SetToken(string token);

		abstract public string GetToken();

		abstract public void SetLastUpdate(long lastUpdate);

		abstract public long GetLastUpdate();

		abstract public void SetDangerState (int level, bool state);

		abstract public bool GetDangerState(int level);

		abstract public void SetCategoryState (int id, bool state);

		abstract public bool GetCategoryState(int id);

        abstract public void SetFilterDangerState(int level, bool state);

        abstract public bool GetFilterDangerState(int level);

        abstract public void SetFilterCategoryState(int id, bool state);

        abstract public bool GetFilterCategoryState(int id);

        abstract public void SetDebugTag(Boolean value);

        abstract public Boolean GetDebugTag();
    }
}


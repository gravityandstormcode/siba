﻿using System;
using System.Collections.Generic;

namespace SiBa
{
    public class SiBaArticlesFilter
    {
        protected List<SiBaArticle> _filteredArticles;
        protected Dictionary<int, bool> _categories;
        protected bool[] _dangerStates;

        public SiBaArticlesFilter()
        {
        }

        public List<SiBaArticle> FilteredArticles
        {
            get {
                return _filteredArticles;
             }
        }

        private void InitFilters()
        {
            SiBaSettings settings = SiBaSettings.GetInstance();
            _dangerStates = new bool[] {
                settings.GetFilterDangerState(0),
                settings.GetFilterDangerState(1),
                settings.GetFilterDangerState(2)
            };

            List<SiBaCategory> categories = SiBaCategories.GetInstance().Categories;

            _categories = new Dictionary<int, bool>();
            if (categories == null) { return; }
            foreach (SiBaCategory category in categories)
            {
                _categories.Add(category.Id, settings.GetFilterCategoryState(category.Id));
            }
        }

        public void Filter()
        {
            try
            {
                List<SiBaArticle> articles = SiBaArticles.GetInstance().Articles;
                if (articles != null)
                {
                    this.InitFilters();

                    this._filteredArticles = new List<SiBaArticle>();
                    foreach (SiBaArticle article in articles)
                    {
                        if (this._dangerStates[article.Level])
                        {
                            List<int> categories = article.Categories;
                            foreach (int category in categories)
                            {
                                if (this._categories != null)
                                {
                                    if (this._categories.ContainsKey(category))
                                    {
                                        if (this._categories[category]) { 
                                            this._filteredArticles.Add(article);
                                            break;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            } catch (Exception e) {
               System.Diagnostics.Debug.WriteLine("Exception:"+e.Message);
            }
        }
    }
}

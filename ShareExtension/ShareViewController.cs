﻿using System;
using CoreFoundation;
using Foundation;
using MobileCoreServices;
using Social;

namespace ShareExtension
{

    public class MySessionDelegate : NSUrlSessionDownloadDelegate
    {
        public override void DidWriteData(NSUrlSession session, NSUrlSessionDownloadTask downloadTask, long bytesWritten, long totalBytesWritten, long totalBytesExpectedToWrite)
        {
        }

        public override void DidFinishDownloading(NSUrlSession session, NSUrlSessionDownloadTask downloadTask, NSUrl location)
        {
        }

    }

    public class MyConnectionDelegate : NSUrlConnectionDelegate
    {
        public MyConnectionDelegate()
        {
        }

    }

    public partial class ShareViewController : SLComposeServiceViewController
    {

        string code = "QU6wT75eaPkeXUDahNfjXra6SysMMtgGhPuvcfbsv50pl6DfvDIGUg==";
        string url = "https://sibafuntionsproduction.azurewebsites.net/api/GridSend?";
        // string code = "...INSERT AZURE TOKEN HERE ..."; // https://portal.azure.com/
        // string url = "...INSERT AZURE FUNCTION URL HERE (INCLUDING THE ? AT THE END)"; // https://portal.azure.com/
        string to = "siba@dsin.de";

        MySessionDelegate delegateObj = new MySessionDelegate();
        protected ShareViewController(IntPtr handle) : base(handle)
        {
            // Note: this .ctor should not contain any initialization logic.
        }

        public override void DidReceiveMemoryWarning()
        {
            // Releases the view if it doesn't have a superview.
            base.DidReceiveMemoryWarning();

            // Release any cached data, images, etc that aren't in use.
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();
            // Do any additional setup after loading the view.
        }

        public override void ViewDidAppear(bool animated)
        {
            base.ViewDidAppear(animated);
            this.NavigationController.NavigationBar.TopItem.RightBarButtonItem.Title = "Abschicken"; // navigationBar.topItem?.rightBarButtonItem?.title = "Save"
            this.NavigationController.NavigationBar.TopItem.LeftBarButtonItem.Title = "Abbrechen"; 
        }

        public override bool IsContentValid()
        {
            // Do validation of contentText and/or NSExtensionContext attachments here
            return true;
        }

        public override void DidSelectPost()
        {
            // This is called after the user selects Post. Do the upload of contentText and/or NSExtensionContext attachments.
            NSExtensionItem[] InputItems = ExtensionContext.InputItems;
            if (InputItems.Length > 0)
            {
                NSExtensionItem Item = InputItems[0];
                NSAttributedString ItemContentText = Item.AttributedContentText;
                NSItemProvider[] Providers = Item.Attachments;
                foreach (NSItemProvider Provider in Providers)
                {
                    //NSItemProvider Provider = Providers[0];

                    Provider.LoadItem(UTType.URL, null, (NSObject payload, NSError error) =>
                    {
                        if (error != null)
                        {
                            // Check if it was a text
                            Provider.LoadItem(UTType.Text, null, (NSObject payloadText, NSError errorText) =>
                            {
                                if (errorText != null)
                                {
                                    // Check if it is a webpage
                                    Provider.LoadItem(UTType.Image, null, (NSObject payloadImage, NSError errorImage) =>
                                    {
                                        if (errorImage != null)
                                        {
                                            if (payloadImage == null)
                                            {
                                                return;
                                            }

                                        }
                                    });
                                    return;
                                }
                                DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, 1000000000), () =>
                                {
                                    if (payloadText == null)
                                    {
                                        return;
                                    }
                                    string payloadString = payloadText.ToString();
                                    if (payloadString == "") {
                                        return;
                                    }
                                    string payloadBodyText = "";
                                    try
                                    {
                                        payloadBodyText = ItemContentText.Value;
                                    }
                                    catch (Exception e)
                                    {
                                        payloadBodyText = "";
                                    }

                                    string from = "siba@geraldhoops.de";
                                    string link = Uri.EscapeUriString(payloadString);
                                    string title = "Über die App geteilter Text";
                                    string body = Uri.EscapeUriString(payloadBodyText);

                                    string RestUrl = url + "code=" + code + "&from=" + from + "&to=" + to + "&link=" + Uri.EscapeUriString(link) + "&title=" + Uri.EscapeUriString(title) + "&message=" + Uri.EscapeUriString(body);
                                    NSUrl urlGridSend = new NSUrl(RestUrl);
                                    NSMutableUrlRequest request = new NSMutableUrlRequest(urlGridSend);

                                    NSUrlConnection connection = new NSUrlConnection(request, new MyConnectionDelegate());
                                    connection.Start();
                                    ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);
                                });
                            });
                            return;
                        }

                        DispatchQueue.MainQueue.DispatchAfter(new DispatchTime(DispatchTime.Now, 1000000000), () =>
                        {
                            if (payload == null)
                            {
                                return;
                            }
                            string payloadString = payload.ToString();
                            string payloadBodyText = "";
                            try
                            {
                                payloadBodyText = ItemContentText.Value;
                            }
                            catch (Exception e)
                            {
                                payloadBodyText = "";
                            }

                            // Needs Code

                            string from = "siba@geraldhoops.de";
                            string link = Uri.EscapeUriString(payloadString);
                            string title = "Über die App geteilter Link";
                            string body = Uri.EscapeUriString(payloadBodyText);

                            string RestUrl = url + "code=" + code + "&from=" + from + "&to=" + to + "&link=" + Uri.EscapeUriString(link) + "&title=" + Uri.EscapeUriString(title) + "&message=" + Uri.EscapeUriString(body);
                            NSUrl urlGridSend = new NSUrl(RestUrl);
                            NSMutableUrlRequest request = new NSMutableUrlRequest(urlGridSend);

                            NSUrlConnection connection = new NSUrlConnection(request, new MyConnectionDelegate());
                            connection.Start();
                            ExtensionContext.CompleteRequest(new NSExtensionItem[0], null);

                        });
                    });
                } // end for each provider
            } // end if lenght > 0
        }


        private static void CompletionHandler(Boolean result)
        {
        }

        public override SLComposeSheetConfigurationItem[] GetConfigurationItems()
        {
            // To add configuration options via table cells at the bottom of the sheet, return an array of SLComposeSheetConfigurationItem here.
            return new SLComposeSheetConfigurationItem[0];
        }
    }
}

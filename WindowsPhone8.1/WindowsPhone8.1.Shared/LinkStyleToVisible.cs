﻿using SiBa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml;
using Windows.UI.Xaml.Data;

namespace SiBa.UWP.Shared
{
    class LinkStyleToVisible : IValueConverter
    {
        public object Convert(object value, Type targetType,
            object parameter, string culture)
        {
            return (Link.LinkStyle)value == Link.LinkStyle.TitleLinkNoLink ? Visibility.Collapsed : Visibility.Visible;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}

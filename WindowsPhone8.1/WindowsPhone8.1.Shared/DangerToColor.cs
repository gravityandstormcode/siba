﻿using SiBa;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Windows.UI.Xaml.Data;
using Windows.UI.Xaml.Media;

namespace SiBa.UWP.Shared
{
    class DangerToColor : IValueConverter  
    {
        public object Convert(object value, Type targetType,
            object parameter, string culture)
        {
            switch ((int)value)
            {
                case 0:
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(0xff, 0x00, 0xb4, 0x8b)); 
                case 1:
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(0xff, 0xdc, 0xbb, 0x47));
                case 2:
                    return new SolidColorBrush(Windows.UI.Color.FromArgb(0xff, 0xf3, 0x58, 0x58));
            }
            return Windows.UI.Colors.White;
        }

        public object ConvertBack(object value, Type targetType,
            object parameter, string culture)
        {
            throw new NotImplementedException();
        }
    }
}
